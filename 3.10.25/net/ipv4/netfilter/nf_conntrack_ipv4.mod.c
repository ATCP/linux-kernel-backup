#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd0298b8b, __VMLINUX_SYMBOL_STR(proc_dointvec_minmax) },
	{ 0x609f1c7e, __VMLINUX_SYMBOL_STR(synchronize_net) },
	{ 0xa7c3e250, __VMLINUX_SYMBOL_STR(nf_conntrack_in) },
	{ 0x3ef15cfe, __VMLINUX_SYMBOL_STR(nf_conntrack_l3proto_generic) },
	{ 0x624fd50, __VMLINUX_SYMBOL_STR(__nf_ct_refresh_acct) },
	{ 0xe02819f3, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_pernet_register) },
	{ 0xd067fc5c, __VMLINUX_SYMBOL_STR(proc_dointvec) },
	{ 0x5a92c21c, __VMLINUX_SYMBOL_STR(nf_ct_get_tuplepr) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x90ce81d5, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0x448eac3e, __VMLINUX_SYMBOL_STR(kmemdup) },
	{ 0xc0240dd4, __VMLINUX_SYMBOL_STR(nf_conntrack_set_hashsize) },
	{ 0x54a7bdf5, __VMLINUX_SYMBOL_STR(nf_ct_deliver_cached_events) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0xea054b22, __VMLINUX_SYMBOL_STR(nla_policy_len) },
	{ 0x51312b88, __VMLINUX_SYMBOL_STR(nf_conntrack_l4proto_tcp4) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xad56b741, __VMLINUX_SYMBOL_STR(nf_ip_checksum) },
	{ 0xd10788db, __VMLINUX_SYMBOL_STR(nf_log_packet) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0xdaa3c570, __VMLINUX_SYMBOL_STR(print_tuple) },
	{ 0xb9d1f1bc, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_unregister) },
	{ 0xaef6cd12, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0xdb586915, __VMLINUX_SYMBOL_STR(nf_ct_invert_tuple) },
	{ 0x33d1a2e1, __VMLINUX_SYMBOL_STR(__nf_conntrack_confirm) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x8bedac1a, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_pernet_unregister) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xc18ac88d, __VMLINUX_SYMBOL_STR(nf_ct_expect_hsize) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0x8fe2c889, __VMLINUX_SYMBOL_STR(seq_putc) },
	{ 0xf39b35d2, __VMLINUX_SYMBOL_STR(nf_ct_l4proto_register) },
	{ 0x4c3db7d4, __VMLINUX_SYMBOL_STR(seq_release_net) },
	{ 0x8ffe7e89, __VMLINUX_SYMBOL_STR(nf_conntrack_htable_size) },
	{ 0x54830a14, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_unregister) },
	{ 0xdbeba7fe, __VMLINUX_SYMBOL_STR(nf_unregister_hooks) },
	{ 0x67d8d35, __VMLINUX_SYMBOL_STR(security_release_secctx) },
	{ 0xefdd70ce, __VMLINUX_SYMBOL_STR(security_secid_to_secctx) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x76a495c1, __VMLINUX_SYMBOL_STR(cpu_possible_mask) },
	{ 0x6e224a7a, __VMLINUX_SYMBOL_STR(need_conntrack) },
	{ 0xe200d2d5, __VMLINUX_SYMBOL_STR(param_get_uint) },
	{ 0x81c1ee8b, __VMLINUX_SYMBOL_STR(nf_unregister_sockopt) },
	{ 0x9fdd71b, __VMLINUX_SYMBOL_STR(nf_ct_l3protos) },
	{ 0xca8210d5, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_pernet_register) },
	{ 0xad4fe501, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0x32047ad5, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0x2a18c74, __VMLINUX_SYMBOL_STR(nf_conntrack_destroy) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0x7f76e4a5, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x33bf8734, __VMLINUX_SYMBOL_STR(seq_open_net) },
	{ 0x6b6c3d10, __VMLINUX_SYMBOL_STR(nf_defrag_ipv4_enable) },
	{ 0x11c6422c, __VMLINUX_SYMBOL_STR(nf_register_hooks) },
	{ 0xf38bcdf3, __VMLINUX_SYMBOL_STR(nf_conntrack_max) },
	{ 0x20479200, __VMLINUX_SYMBOL_STR(nf_nat_seq_adjust_hook) },
	{ 0x72335d36, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_pernet_unregister) },
	{ 0x746ba7bb, __VMLINUX_SYMBOL_STR(nf_conntrack_l4proto_udp4) },
	{ 0x6b8402af, __VMLINUX_SYMBOL_STR(nf_register_sockopt) },
	{ 0x98b6dac, __VMLINUX_SYMBOL_STR(proc_dointvec_jiffies) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xfe188b4b, __VMLINUX_SYMBOL_STR(seq_print_acct) },
	{ 0x4193963, __VMLINUX_SYMBOL_STR(nf_ct_l3proto_register) },
	{ 0xcaa5172f, __VMLINUX_SYMBOL_STR(nf_conntrack_find_get) },
	{ 0xa8de0646, __VMLINUX_SYMBOL_STR(__nf_ct_l4proto_find) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=nf_conntrack,nf_defrag_ipv4";


MODULE_INFO(srcversion, "CF715113224CC90655FA96F");
