/*
 * tcpccph - The host endpoint implemetation for Centralized Control Protocol stack.
 *
 * Copyright (C) 2014, Perth Charles <zhongbincharles@gmail.com>
 */


#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/socket.h>
#include <linux/tcp.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/ktime.h>
#include <linux/time.h>
#include <net/net_namespace.h>

#include <linux/sched.h>   // wake_up_process()
#include <linux/kthread.h> // kthread_create(), kthread_run()
#include <linux/err.h>     // IS_ERR(), PTR_ERR()

#include <net/tcp.h>
#include <net/sock.h>
#include <net/tcp_states.h>

#include "tcp_flowmsg.h"

MODULE_AUTHOR("Perth Charles <zhongbincharles@gmail.com>");
MODULE_DESCRIPTION("TCP Centralized Control Protocol stack");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.3");

static unsigned int bufsize __read_mostly = 4096;
MODULE_PARM_DESC(bufsize, "Buffer size for messages.");
module_param(bufsize, uint, 0);

static const char procname[] = "tcpccph";

static struct ccph_buf ccph_order;   // write buffer, order message received from controller
static struct ccph_buf ccph_update;  // read buffer, update message collected by host

static struct task_struct *ccph_handler;  

static int ccph_order_handler(void *date)
{
    struct sock *sk;
    struct flow_msg *cur;
    int error = 0;

    pr_info("ccph_order_handler() is called.\n");
    while (1) {
        error = wait_event_interruptible(ccph_order.wait, 
                                         (ccph_buf_used(&ccph_order, bufsize) > 0) | kthread_should_stop());
        if (error == -EINTR) {
            pr_info("ccph_order_handler() Interrupted system call. continue...\n");
            continue;
        } else if (error) {
            pr_info("ccph_order_handler() wait error:%d.\n", error);
            break;
        } else if (ccph_buf_used(&ccph_order, bufsize) <= 0) {
            pr_info("ccph_order_handler() killed by kthread_stop() with err = %d.\n", error);
            break;
        }

        spin_lock(&ccph_order.lock);  // spin_lock() or spin_lock_bh() ?

        cur = ccph_buf_tail(&ccph_order);

        sk = __inet_lookup_established((struct net *)(cur->sock_net), &tcp_hashinfo,
                                       cur->daddr, cur->dport,
                                       cur->saddr, cur->snum,
                                       (int)(cur->dev_index));
        if (sk) {
            struct tcp_sock *tp = tcp_sk(sk);
            switch(cur->flag) {
                case SET_CWND: {
                    u32 prior_cwnd = tp->snd_cwnd;
                    tp->snd_cwnd = (cur->value < tp->snd_cwnd_clamp) ? cur->value : tp->snd_cwnd_clamp;
                    pr_info("setting cwnd[%u] to %u. cur->value = %u, clamp = %u\n", prior_cwnd, tp->snd_cwnd, cur->value, tp->snd_cwnd_clamp);
                    break;
                }
                default: {
                    pr_info("ccph_order_hanlder(): unsupportted order type.\n");
                }
            }
        } else {
            pr_info("ccph_order_hanlder(): sk not found!\n");
        }
        ccph_buf_del(&ccph_order, bufsize);

        spin_unlock(&ccph_order.lock);
    }

    return error;
}

static int ccph_proc_open(struct inode *inode, struct file *file)
{
    ccph_buf_init(&ccph_order);
    ccph_buf_init(&ccph_update);

    pr_info("open success.\n");
    return 0;
}

// TODO1: add jprobe function to collect updates
// static int j..

/* TODO2: change to binary read after a few tests */
static ssize_t ccph_proc_read(struct file *file, char __user *buf,
                              size_t len, loff_t *ppos)
{
    int error = 0;
    size_t cnt = 0;

    if (!buf || len < sizeof(struct flow_msg)) {
        return -EINVAL;
    }

    while (cnt < len) {
        char tbuf[256];
        int width;
        
        pr_info("read start to wait.\n");
        error = wait_event_interruptible(ccph_update.wait, ccph_buf_used(&ccph_update, bufsize) > 0);
        pr_info("read end wait.\n");
        if (error) {
            pr_info("[read] wait_event_interrruptible() error: %d\n", error);
            break;
        }

        spin_lock_bh(&ccph_update.lock);
        if (ccph_update.head == ccph_update.tail) {
            /* multiple readers rate ? */
            spin_unlock_bh(&ccph_update.lock);
            continue;
        }

        width = flowmsg_readone(&ccph_update, tbuf, sizeof(tbuf));
        if (cnt + width < len) {
            ccph_buf_del(&ccph_update, bufsize);
        }
        spin_unlock_bh(&ccph_update.lock);

        if (cnt + width >= len) {
            break;
        }

        if (copy_to_user(buf + cnt, tbuf, width)) {
            return -EFAULT;
        }
        cnt += width;
    }

    return cnt == 0 ? error : cnt;
}

static ssize_t ccph_proc_write(struct file *file, const char __user *buf,
                               size_t len, loff_t *ppos)
{
    size_t cnt = 0;
    struct flow_msg *cur;
    int error = 0;

    while (cnt + sizeof(struct flow_msg) <= len) {
        spin_lock_bh(&ccph_order.lock);
        cur = ccph_buf_head(&ccph_order);

        if (ccph_buf_avail(&ccph_order, bufsize) > 1) {
            if (copy_from_user(cur, buf + cnt, sizeof(struct flow_msg))) {
                spin_unlock_bh(&ccph_order.lock);
                return -EFAULT;
            }
            cnt += sizeof(struct flow_msg);
            ccph_buf_add(&ccph_order, bufsize);
        } else {
            error = -ENOSPC;
            spin_unlock_bh(&ccph_order.lock);
            break;
        } 
        spin_unlock_bh(&ccph_order.lock);
    }
 
    wake_up(&ccph_order.wait);

    return cnt == 0 ? error : cnt;
}

static int ccph_proc_close(struct inode *inode, struct file *file)
{
    return 0;  /* success */
} 

static const struct file_operations ccph_proc_fops = {
    .owner   = THIS_MODULE,
    .open    = ccph_proc_open,
    .read    = ccph_proc_read,
    .write   = ccph_proc_write,
    .release = ccph_proc_close,  // stop the daemon thread: ccph_order_handler
    .llseek  = noop_llseek,
};

static __init int tcp_ccph_init(void)
{
    int ret = -ENOMEM;

    init_waitqueue_head(&ccph_order.wait);
    spin_lock_init(&ccph_order.lock);

    init_waitqueue_head(&ccph_update.wait);
    spin_lock_init(&ccph_update.lock);

    if (bufsize == 0) {
         return -EINVAL;
    }
    bufsize = roundup_pow_of_two(bufsize);

    ccph_order.buf = kcalloc(bufsize, sizeof(struct flow_msg), GFP_KERNEL);
    if (!ccph_order.buf) {
        goto err0;
    }

    ccph_update.buf = kcalloc(bufsize, sizeof(struct flow_msg), GFP_KERNEL);
    if (!ccph_update.buf) {
        goto err1;
    }

    /* readable and writeable */
    if (!proc_create(procname, S_IRUSR | S_IWUSR, init_net.proc_net, &ccph_proc_fops)) {
        goto err1;
    }

    /* create a daemon thread to handle received orders */
    ccph_handler = kthread_create(ccph_order_handler, NULL, "ccph_order_handler");
    if (IS_ERR(ccph_handler)) {
        pr_info("kthread_create error\n");
        return -EAGAIN;
    }
    if(!wake_up_process(ccph_handler)) {
        pr_info("ccph_hanlder is already running.\n");
    }

    pr_info("tcpccph registered bufsize=%u\n", bufsize);
    return 0;

err1:
    kfree(ccph_update.buf);
err0:
    kfree(ccph_order.buf);
    return ret;
}
module_init(tcp_ccph_init);

static __exit void tcp_ccph_exit(void)
{
    // stop the daemon thread: ccph_order_handler
    int ret = 0;
    ret = kthread_stop(ccph_handler);
    ccph_handler = NULL;
    pr_info("stop ccph_order_hanlder with return=%d\n", ret);

    remove_proc_entry(procname, init_net.proc_net);
    kfree(ccph_order.buf);
    kfree(ccph_update.buf);
    pr_info("tcpccph unregistered\n");
}
module_exit(tcp_ccph_exit);
