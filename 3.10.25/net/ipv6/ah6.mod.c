#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd187d88c, __VMLINUX_SYMBOL_STR(xfrm6_find_1stfragopt) },
	{ 0xff75b077, __VMLINUX_SYMBOL_STR(xfrm6_rcv) },
	{ 0x68dcfdc1, __VMLINUX_SYMBOL_STR(inet6_add_protocol) },
	{ 0xd7c99d0c, __VMLINUX_SYMBOL_STR(xfrm_register_type) },
	{ 0x3b0e3032, __VMLINUX_SYMBOL_STR(__xfrm_state_destroy) },
	{ 0xcf9b4b2b, __VMLINUX_SYMBOL_STR(ip6_update_pmtu) },
	{ 0x70b05fec, __VMLINUX_SYMBOL_STR(ip6_redirect) },
	{ 0xd28d488, __VMLINUX_SYMBOL_STR(xfrm_state_lookup) },
	{ 0xe122af95, __VMLINUX_SYMBOL_STR(xfrm_aalg_get_byname) },
	{ 0xe9f8d76d, __VMLINUX_SYMBOL_STR(crypto_ahash_setkey) },
	{ 0xdef71688, __VMLINUX_SYMBOL_STR(crypto_alloc_ahash) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x9e6f20de, __VMLINUX_SYMBOL_STR(crypto_destroy_tfm) },
	{ 0x4df86757, __VMLINUX_SYMBOL_STR(xfrm_input_resume) },
	{ 0xfc8e62b4, __VMLINUX_SYMBOL_STR(pskb_expand_head) },
	{ 0xce993617, __VMLINUX_SYMBOL_STR(__pskb_pull_tail) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x4fc53fa5, __VMLINUX_SYMBOL_STR(xfrm_output_resume) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x1d8ba650, __VMLINUX_SYMBOL_STR(crypto_ahash_digest) },
	{ 0xbb435892, __VMLINUX_SYMBOL_STR(skb_to_sgvec) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0xe6961bf3, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0xb922be9a, __VMLINUX_SYMBOL_STR(skb_cow_data) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xb0e602eb, __VMLINUX_SYMBOL_STR(memmove) },
	{ 0xa20ce1b8, __VMLINUX_SYMBOL_STR(net_msg_warn) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xcca6a892, __VMLINUX_SYMBOL_STR(xfrm_unregister_type) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x9b4181a3, __VMLINUX_SYMBOL_STR(inet6_del_protocol) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ipv6";


MODULE_INFO(srcversion, "CDCF5C5E16C8FD5A542CC9B");
