#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x67e6478c, __VMLINUX_SYMBOL_STR(register_atm_ioctl) },
	{ 0xd40b5a00, __VMLINUX_SYMBOL_STR(ppp_input_error) },
	{ 0x25ad5d25, __VMLINUX_SYMBOL_STR(ppp_input) },
	{ 0xc1437d38, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0x437d0bd5, __VMLINUX_SYMBOL_STR(ppp_unregister_channel) },
	{ 0x82072614, __VMLINUX_SYMBOL_STR(tasklet_kill) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x5cf25fea, __VMLINUX_SYMBOL_STR(ppp_unit_number) },
	{ 0x9e89cb5d, __VMLINUX_SYMBOL_STR(ppp_channel_index) },
	{ 0x5547eba8, __VMLINUX_SYMBOL_STR(vcc_process_recv_queue) },
	{ 0xee26750e, __VMLINUX_SYMBOL_STR(__module_get) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x596251aa, __VMLINUX_SYMBOL_STR(ppp_register_channel) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x8f9c199c, __VMLINUX_SYMBOL_STR(__get_user_2) },
	{ 0x6729d3df, __VMLINUX_SYMBOL_STR(__get_user_4) },
	{ 0xb2fd5ceb, __VMLINUX_SYMBOL_STR(__put_user_4) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xdb74383d, __VMLINUX_SYMBOL_STR(ppp_output_wakeup) },
	{ 0xfaef0ed, __VMLINUX_SYMBOL_STR(__tasklet_schedule) },
	{ 0xe6961bf3, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x36141349, __VMLINUX_SYMBOL_STR(skb_realloc_headroom) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x1ce107cb, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0x80330411, __VMLINUX_SYMBOL_STR(deregister_atm_ioctl) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=atm,ppp_generic";


MODULE_INFO(srcversion, "A6D8402D614FE916F1EF3C9");
