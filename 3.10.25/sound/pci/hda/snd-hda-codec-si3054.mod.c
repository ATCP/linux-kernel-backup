#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x86e906b2, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_list) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0x3888ac21, __VMLINUX_SYMBOL_STR(snd_ctl_boolean_mono_info) },
	{ 0x369b2a3b, __VMLINUX_SYMBOL_STR(snd_hda_codec_write_cache) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x5528903c, __VMLINUX_SYMBOL_STR(snd_hda_codec_setup_stream) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0xa9d4c058, __VMLINUX_SYMBOL_STR(snd_hda_add_new_ctls) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-pcm,snd-hda-codec,snd";


MODULE_INFO(srcversion, "AEB23A0DBEDF0666646FF4D");
