#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x5fd46b07, __VMLINUX_SYMBOL_STR(ib_send_cm_drep) },
	{ 0x115066a6, __VMLINUX_SYMBOL_STR(ib_send_cm_rtu) },
	{ 0x931ee0e0, __VMLINUX_SYMBOL_STR(ib_cm_init_qp_attr) },
	{ 0x754d9d38, __VMLINUX_SYMBOL_STR(ib_register_client) },
	{ 0xc1955a3f, __VMLINUX_SYMBOL_STR(ib_sa_register_client) },
	{ 0x9f7f06f9, __VMLINUX_SYMBOL_STR(__class_register) },
	{ 0x5f3b3765, __VMLINUX_SYMBOL_STR(srp_attach_transport) },
	{ 0xf471ead9, __VMLINUX_SYMBOL_STR(scsi_target_unblock) },
	{ 0xc85c94ba, __VMLINUX_SYMBOL_STR(scsi_target_block) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x810f7ea6, __VMLINUX_SYMBOL_STR(scsi_scan_target) },
	{ 0x53a26556, __VMLINUX_SYMBOL_STR(srp_rport_add) },
	{ 0x51973243, __VMLINUX_SYMBOL_STR(scsi_add_host_with_dma) },
	{ 0xceaa2bb0, __VMLINUX_SYMBOL_STR(ib_query_gid) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x85df9b6c, __VMLINUX_SYMBOL_STR(strsep) },
	{ 0x4e3567f7, __VMLINUX_SYMBOL_STR(match_int) },
	{ 0xad0413d4, __VMLINUX_SYMBOL_STR(match_hex) },
	{ 0x20000329, __VMLINUX_SYMBOL_STR(simple_strtoul) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x61b7b126, __VMLINUX_SYMBOL_STR(simple_strtoull) },
	{ 0xacf4d843, __VMLINUX_SYMBOL_STR(match_strdup) },
	{ 0x44e9a829, __VMLINUX_SYMBOL_STR(match_token) },
	{ 0xc499ae1e, __VMLINUX_SYMBOL_STR(kstrdup) },
	{ 0xaddb8893, __VMLINUX_SYMBOL_STR(scsi_host_alloc) },
	{ 0x3fde1c2e, __VMLINUX_SYMBOL_STR(ib_send_cm_req) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0x17440307, __VMLINUX_SYMBOL_STR(ib_sa_path_rec_get) },
	{ 0x53f6ffbc, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x2615eddf, __VMLINUX_SYMBOL_STR(ib_set_client_data) },
	{ 0x7e08f3f1, __VMLINUX_SYMBOL_STR(device_create_file) },
	{ 0xc61c7f2e, __VMLINUX_SYMBOL_STR(device_register) },
	{ 0x291f89b7, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xbd5152bc, __VMLINUX_SYMBOL_STR(ib_create_fmr_pool) },
	{ 0xa3db70d, __VMLINUX_SYMBOL_STR(ib_get_dma_mr) },
	{ 0x4a2fc29b, __VMLINUX_SYMBOL_STR(ib_alloc_pd) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x25db2868, __VMLINUX_SYMBOL_STR(ib_query_device) },
	{ 0x28fd2cd3, __VMLINUX_SYMBOL_STR(scsi_host_put) },
	{ 0x5af82fa6, __VMLINUX_SYMBOL_STR(scsi_remove_host) },
	{ 0x199a3baf, __VMLINUX_SYMBOL_STR(srp_remove_host) },
	{ 0x843a078e, __VMLINUX_SYMBOL_STR(device_remove_file) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x113e3b15, __VMLINUX_SYMBOL_STR(ib_send_cm_dreq) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xeed44943, __VMLINUX_SYMBOL_STR(ib_destroy_cm_id) },
	{ 0x9e023cd9, __VMLINUX_SYMBOL_STR(ib_create_cm_id) },
	{ 0xf036d1f6, __VMLINUX_SYMBOL_STR(ib_modify_qp) },
	{ 0x32c3ab60, __VMLINUX_SYMBOL_STR(ib_find_pkey) },
	{ 0xb09e8dad, __VMLINUX_SYMBOL_STR(ib_create_qp) },
	{ 0x87dcd430, __VMLINUX_SYMBOL_STR(ib_create_cq) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xfbc89c16, __VMLINUX_SYMBOL_STR(ib_destroy_cq) },
	{ 0xa72c5e19, __VMLINUX_SYMBOL_STR(ib_destroy_qp) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x48b8bb5e, __VMLINUX_SYMBOL_STR(ib_fmr_pool_map_phys) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x67d238dd, __VMLINUX_SYMBOL_STR(ib_fmr_pool_unmap) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xe381912f, __VMLINUX_SYMBOL_STR(blk_queue_rq_timeout) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x3bfe8d2c, __VMLINUX_SYMBOL_STR(ib_dealloc_pd) },
	{ 0xad5377f9, __VMLINUX_SYMBOL_STR(ib_dereg_mr) },
	{ 0xc32b59ff, __VMLINUX_SYMBOL_STR(ib_destroy_fmr_pool) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x6d0aba34, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0xb489f380, __VMLINUX_SYMBOL_STR(device_unregister) },
	{ 0x6affeb4c, __VMLINUX_SYMBOL_STR(ib_get_client_data) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x131db64a, __VMLINUX_SYMBOL_STR(system_long_wq) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0xefe254f5, __VMLINUX_SYMBOL_STR(srp_release_transport) },
	{ 0x42c57234, __VMLINUX_SYMBOL_STR(class_unregister) },
	{ 0x2fd0e42f, __VMLINUX_SYMBOL_STR(ib_sa_unregister_client) },
	{ 0x7125c47b, __VMLINUX_SYMBOL_STR(ib_unregister_client) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ib_cm,ib_core,ib_sa,scsi_transport_srp,scsi_mod";


MODULE_INFO(srcversion, "40DFC7BB6456C3A835C7737");
