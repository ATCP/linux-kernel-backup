#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x7e823052, __VMLINUX_SYMBOL_STR(netdev_info) },
	{ 0xf71ea89a, __VMLINUX_SYMBOL_STR(register_candev) },
	{ 0x6c612834, __VMLINUX_SYMBOL_STR(alloc_can_err_skb) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x240c6be6, __VMLINUX_SYMBOL_STR(alloc_candev) },
	{ 0xb435e87, __VMLINUX_SYMBOL_STR(can_bus_off) },
	{ 0x3b6279d7, __VMLINUX_SYMBOL_STR(netif_rx) },
	{ 0xfdfcd50f, __VMLINUX_SYMBOL_STR(close_candev) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x35887799, __VMLINUX_SYMBOL_STR(netdev_printk) },
	{ 0x1902adf, __VMLINUX_SYMBOL_STR(netpoll_trap) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0xb3960dca, __VMLINUX_SYMBOL_STR(free_candev) },
	{ 0x85a7445b, __VMLINUX_SYMBOL_STR(unregister_candev) },
	{ 0xf36d2686, __VMLINUX_SYMBOL_STR(alloc_can_skb) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0xa614dbb1, __VMLINUX_SYMBOL_STR(netdev_err) },
	{ 0x2a37c8fb, __VMLINUX_SYMBOL_STR(open_candev) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x3c39aced, __VMLINUX_SYMBOL_STR(netdev_warn) },
	{ 0xfcaea6c5, __VMLINUX_SYMBOL_STR(can_get_echo_skb) },
	{ 0x5d7a5723, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0x6c9d87c4, __VMLINUX_SYMBOL_STR(can_put_echo_skb) },
	{ 0x61322d05, __VMLINUX_SYMBOL_STR(can_free_echo_skb) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=can-dev";


MODULE_INFO(srcversion, "2442D123CA80C4D19A3D08C");
