#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0x2a37c8fb, __VMLINUX_SYMBOL_STR(open_candev) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xfdfcd50f, __VMLINUX_SYMBOL_STR(close_candev) },
	{ 0xf71ea89a, __VMLINUX_SYMBOL_STR(register_candev) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x240c6be6, __VMLINUX_SYMBOL_STR(alloc_candev) },
	{ 0xfe5c436c, __VMLINUX_SYMBOL_STR(usb_bulk_msg) },
	{ 0xf36d2686, __VMLINUX_SYMBOL_STR(alloc_can_skb) },
	{ 0x3b6279d7, __VMLINUX_SYMBOL_STR(netif_rx) },
	{ 0xb435e87, __VMLINUX_SYMBOL_STR(can_bus_off) },
	{ 0x6c612834, __VMLINUX_SYMBOL_STR(alloc_can_err_skb) },
	{ 0x5d7a5723, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0x1902adf, __VMLINUX_SYMBOL_STR(netpoll_trap) },
	{ 0xfcaea6c5, __VMLINUX_SYMBOL_STR(can_get_echo_skb) },
	{ 0x7e823052, __VMLINUX_SYMBOL_STR(netdev_info) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xbe82da40, __VMLINUX_SYMBOL_STR(netif_device_detach) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0x61322d05, __VMLINUX_SYMBOL_STR(can_free_echo_skb) },
	{ 0xf503b80a, __VMLINUX_SYMBOL_STR(usb_submit_urb) },
	{ 0x6c9d87c4, __VMLINUX_SYMBOL_STR(can_put_echo_skb) },
	{ 0x7d0aa01f, __VMLINUX_SYMBOL_STR(usb_anchor_urb) },
	{ 0x3c39aced, __VMLINUX_SYMBOL_STR(netdev_warn) },
	{ 0x1a884056, __VMLINUX_SYMBOL_STR(usb_free_coherent) },
	{ 0xc2cea7ff, __VMLINUX_SYMBOL_STR(usb_unanchor_urb) },
	{ 0xd24fcdf9, __VMLINUX_SYMBOL_STR(usb_alloc_coherent) },
	{ 0xa614dbb1, __VMLINUX_SYMBOL_STR(netdev_err) },
	{ 0xfc44da0f, __VMLINUX_SYMBOL_STR(usb_alloc_urb) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x32eaa242, __VMLINUX_SYMBOL_STR(usb_free_urb) },
	{ 0xb3960dca, __VMLINUX_SYMBOL_STR(free_candev) },
	{ 0x1c60d5c9, __VMLINUX_SYMBOL_STR(unregister_netdev) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x8b68ce9f, __VMLINUX_SYMBOL_STR(usb_kill_anchored_urbs) },
	{ 0x3294949b, __VMLINUX_SYMBOL_STR(usb_unlink_urb) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=can-dev";

MODULE_ALIAS("usb:v12D6p0444d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "E4FA651567C3A330A6CD192");
