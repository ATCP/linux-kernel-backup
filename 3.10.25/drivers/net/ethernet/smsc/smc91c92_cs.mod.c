#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x9d862bb8, __VMLINUX_SYMBOL_STR(eth_change_mtu) },
	{ 0x1358f7b9, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0xb06acc77, __VMLINUX_SYMBOL_STR(eth_mac_addr) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xc988e6a, __VMLINUX_SYMBOL_STR(pcmcia_register_driver) },
	{ 0x742f5665, __VMLINUX_SYMBOL_STR(pcmcia_parse_tuple) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x6877b859, __VMLINUX_SYMBOL_STR(register_netdev) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x627c7f40, __VMLINUX_SYMBOL_STR(pcmcia_get_tuple) },
	{ 0x8b184907, __VMLINUX_SYMBOL_STR(pcmcia_get_mac_from_cis) },
	{ 0x9ad2dbd6, __VMLINUX_SYMBOL_STR(pcmcia_loop_tuple) },
	{ 0x39944dcb, __VMLINUX_SYMBOL_STR(dev_notice) },
	{ 0x681ef8d, __VMLINUX_SYMBOL_STR(pcmcia_enable_device) },
	{ 0x6d0b12c2, __VMLINUX_SYMBOL_STR(pcmcia_request_irq) },
	{ 0x9d4af00e, __VMLINUX_SYMBOL_STR(pcmcia_map_mem_page) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0xfd5c9fc3, __VMLINUX_SYMBOL_STR(pcmcia_request_window) },
	{ 0x4c9581a4, __VMLINUX_SYMBOL_STR(pcmcia_loop_config) },
	{ 0xf71a9fee, __VMLINUX_SYMBOL_STR(alloc_etherdev_mqs) },
	{ 0xa3baba49, __VMLINUX_SYMBOL_STR(pcmcia_request_io) },
	{ 0x3b6279d7, __VMLINUX_SYMBOL_STR(netif_rx) },
	{ 0x5d6e6cc0, __VMLINUX_SYMBOL_STR(eth_type_trans) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x2ef407d0, __VMLINUX_SYMBOL_STR(__netdev_alloc_skb) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xd9605d4c, __VMLINUX_SYMBOL_STR(add_timer) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x92e68c17, __VMLINUX_SYMBOL_STR(pcmcia_dev_present) },
	{ 0x5ab0e2a4, __VMLINUX_SYMBOL_STR(pcmcia_fixup_iowidth) },
	{ 0x6f0036d9, __VMLINUX_SYMBOL_STR(del_timer_sync) },
	{ 0x33d53d62, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0x35887799, __VMLINUX_SYMBOL_STR(netdev_printk) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xe82b9e6e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_irq) },
	{ 0x3c39aced, __VMLINUX_SYMBOL_STR(netdev_warn) },
	{ 0xa614dbb1, __VMLINUX_SYMBOL_STR(netdev_err) },
	{ 0x67a773e9, __VMLINUX_SYMBOL_STR(generic_mii_ioctl) },
	{ 0x7e823052, __VMLINUX_SYMBOL_STR(netdev_info) },
	{ 0x5d7a5723, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0x1902adf, __VMLINUX_SYMBOL_STR(netpoll_trap) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x2e008fc, __VMLINUX_SYMBOL_STR(netdev_notice) },
	{ 0xfd49696, __VMLINUX_SYMBOL_STR(mii_ethtool_gset) },
	{ 0x26819c5f, __VMLINUX_SYMBOL_STR(mii_ethtool_sset) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x713dcff7, __VMLINUX_SYMBOL_STR(mii_nway_restart) },
	{ 0x952a2639, __VMLINUX_SYMBOL_STR(mii_link_ok) },
	{ 0x236a3f8c, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0x1c60d5c9, __VMLINUX_SYMBOL_STR(unregister_netdev) },
	{ 0x78b40382, __VMLINUX_SYMBOL_STR(pcmcia_disable_device) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0xbe82da40, __VMLINUX_SYMBOL_STR(netif_device_detach) },
	{ 0xf7f25d3f, __VMLINUX_SYMBOL_STR(netif_device_attach) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xfaf98462, __VMLINUX_SYMBOL_STR(bitrev32) },
	{ 0x802d0e93, __VMLINUX_SYMBOL_STR(crc32_le) },
	{ 0x75c3f17b, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x2e526f67, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xf88db0f2, __VMLINUX_SYMBOL_STR(pcmcia_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mii";

MODULE_ALIAS("pcmcia:m0109c0501f*fn*pfn00pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m0140c000Af*fn*pfn00pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00paF510DB04pb04CD2988pc46A52D63pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00paF510DB04pb0143B773pc46A52D63pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00paF510DB04pb856D66C8pcBD6C43EFpd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00paF510DB04pb52D21E1EpcBD6C43EFpd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00paDD9989BEpb662C394Cpc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00paF510DB04pb52D21E1Epc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00pa0C2F80CDpb656947B9pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn00pa0C2F80CDpbDC9BA5EDpc*pd*");
MODULE_ALIAS("pcmcia:m016Cc0020f*fn00pfn*pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m016Cc0023f*fn*pfn*pa*pb*pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa23C78A9Dpb00B2E941pcCEF397FBpd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa78F308DCpbDCEA68BCpc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*paE59365C8pb6A2161D1pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa6A26D1CFpbC16CE9C5pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa58D93FC4pb244734E9pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa33234748pb3C95B953pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*paA2CD8E6Dpb42DA662Apc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa0C2F80CDpbB3466314pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa0C2F80CDpb194B650Apc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa281F1C5DpbDCEA68BCpc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*pa4EF00B21pb844BE9E9pc*pd*");
MODULE_ALIAS("pcmcia:m*c*f*fn*pfn*paC4F8B18Bpb4A0EEB2Dpc*pd*");

MODULE_INFO(srcversion, "BC2214DFC8A9BF05E1D6117");
