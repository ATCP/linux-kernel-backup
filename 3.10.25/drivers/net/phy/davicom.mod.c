#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x34d425ec, __VMLINUX_SYMBOL_STR(genphy_read_status) },
	{ 0x767f0a78, __VMLINUX_SYMBOL_STR(phy_drivers_register) },
	{ 0xa2838db8, __VMLINUX_SYMBOL_STR(genphy_config_aneg) },
	{ 0x8393b6a6, __VMLINUX_SYMBOL_STR(mdiobus_write) },
	{ 0x18ec44cc, __VMLINUX_SYMBOL_STR(mdiobus_read) },
	{ 0x47027c1b, __VMLINUX_SYMBOL_STR(phy_drivers_unregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libphy";

MODULE_ALIAS("mdio:????000110000001101110001000????");
MODULE_ALIAS("mdio:????000110000001101110001010????");
MODULE_ALIAS("mdio:????000000011000000110111000????");

MODULE_INFO(srcversion, "E5510E52D35ECC1AB257B75");
