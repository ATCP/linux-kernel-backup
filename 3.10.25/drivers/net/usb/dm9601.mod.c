#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb7b980d1, __VMLINUX_SYMBOL_STR(usbnet_resume) },
	{ 0x5005215, __VMLINUX_SYMBOL_STR(usbnet_suspend) },
	{ 0x109aeb3f, __VMLINUX_SYMBOL_STR(usbnet_disconnect) },
	{ 0xabfb708f, __VMLINUX_SYMBOL_STR(usbnet_probe) },
	{ 0x35ed9f6e, __VMLINUX_SYMBOL_STR(usbnet_nway_reset) },
	{ 0xda66aefd, __VMLINUX_SYMBOL_STR(usbnet_set_msglevel) },
	{ 0x4646b5b3, __VMLINUX_SYMBOL_STR(usbnet_get_msglevel) },
	{ 0xbb382529, __VMLINUX_SYMBOL_STR(usbnet_set_settings) },
	{ 0x3c0a5219, __VMLINUX_SYMBOL_STR(usbnet_get_settings) },
	{ 0xd653a9f7, __VMLINUX_SYMBOL_STR(usbnet_tx_timeout) },
	{ 0xd8656e8b, __VMLINUX_SYMBOL_STR(usbnet_change_mtu) },
	{ 0x1358f7b9, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0xaff94333, __VMLINUX_SYMBOL_STR(usbnet_start_xmit) },
	{ 0x6d9f680, __VMLINUX_SYMBOL_STR(usbnet_stop) },
	{ 0xc06f6c44, __VMLINUX_SYMBOL_STR(usbnet_open) },
	{ 0x713dcff7, __VMLINUX_SYMBOL_STR(mii_nway_restart) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xdca237cf, __VMLINUX_SYMBOL_STR(usbnet_get_endpoints) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0xfaf98462, __VMLINUX_SYMBOL_STR(bitrev32) },
	{ 0x802d0e93, __VMLINUX_SYMBOL_STR(crc32_le) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xa614dbb1, __VMLINUX_SYMBOL_STR(netdev_err) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x8139bc74, __VMLINUX_SYMBOL_STR(usbnet_write_cmd) },
	{ 0xed88cff1, __VMLINUX_SYMBOL_STR(usbnet_write_cmd_async) },
	{ 0x8c755968, __VMLINUX_SYMBOL_STR(usbnet_read_cmd) },
	{ 0x67a773e9, __VMLINUX_SYMBOL_STR(generic_mii_ioctl) },
	{ 0xefc7127a, __VMLINUX_SYMBOL_STR(usbnet_get_drvinfo) },
	{ 0x952a2639, __VMLINUX_SYMBOL_STR(mii_link_ok) },
	{ 0xfd49696, __VMLINUX_SYMBOL_STR(mii_ethtool_gset) },
	{ 0xe6ecd5cf, __VMLINUX_SYMBOL_STR(mii_check_media) },
	{ 0x25a85511, __VMLINUX_SYMBOL_STR(usbnet_link_change) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0x1ce107cb, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x3b134b3e, __VMLINUX_SYMBOL_STR(dev_kfree_skb_any) },
	{ 0x86a62306, __VMLINUX_SYMBOL_STR(skb_copy_expand) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usbnet,mii";

MODULE_ALIAS("usb:v07AAp9601d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A46p9601d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A46p6688d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A46p0268d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A46p8515d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A47p9601d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0FE6p8101d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0FE6p9700d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A46p9000d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0A46p9620d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "E40849DEA6DA09036CC46FC");
