#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x46884346, __VMLINUX_SYMBOL_STR(mptscsih_host_attrs) },
	{ 0xe7cebeac, __VMLINUX_SYMBOL_STR(mptscsih_show_info) },
	{ 0x5183dff5, __VMLINUX_SYMBOL_STR(mptscsih_bios_param) },
	{ 0xeb17aa3b, __VMLINUX_SYMBOL_STR(mptscsih_change_queue_depth) },
	{ 0x448cc567, __VMLINUX_SYMBOL_STR(mptscsih_slave_destroy) },
	{ 0xd68e9809, __VMLINUX_SYMBOL_STR(mptscsih_host_reset) },
	{ 0x660e1105, __VMLINUX_SYMBOL_STR(mptscsih_dev_reset) },
	{ 0xf92d9b83, __VMLINUX_SYMBOL_STR(mptscsih_abort) },
	{ 0xc9407759, __VMLINUX_SYMBOL_STR(mptscsih_info) },
	{ 0x30394567, __VMLINUX_SYMBOL_STR(mptscsih_resume) },
	{ 0x694c50e2, __VMLINUX_SYMBOL_STR(mptscsih_suspend) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x2a52ac26, __VMLINUX_SYMBOL_STR(scsi_track_queue_full) },
	{ 0x24a4a6bd, __VMLINUX_SYMBOL_STR(mpt_GetIocState) },
	{ 0xff113415, __VMLINUX_SYMBOL_STR(mptscsih_get_scsi_lookup) },
	{ 0x230885ed, __VMLINUX_SYMBOL_STR(scsi_remove_device) },
	{ 0x94a0cdfe, __VMLINUX_SYMBOL_STR(mpt_raid_phys_disk_pg1) },
	{ 0x44ee295e, __VMLINUX_SYMBOL_STR(mpt_raid_phys_disk_get_num_paths) },
	{ 0xe744bf51, __VMLINUX_SYMBOL_STR(mpt_findImVolumes) },
	{ 0x51973243, __VMLINUX_SYMBOL_STR(scsi_add_host_with_dma) },
	{ 0x6c158401, __VMLINUX_SYMBOL_STR(mptbase_sas_persist_operation) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xaddb8893, __VMLINUX_SYMBOL_STR(scsi_host_alloc) },
	{ 0x28f2c6ce, __VMLINUX_SYMBOL_STR(mptscsih_flush_running_cmds) },
	{ 0x3de027e4, __VMLINUX_SYMBOL_STR(mpt_attach) },
	{ 0xecd30647, __VMLINUX_SYMBOL_STR(scsi_add_device) },
	{ 0x83338307, __VMLINUX_SYMBOL_STR(scsi_device_put) },
	{ 0x6c95f70b, __VMLINUX_SYMBOL_STR(scsi_device_lookup) },
	{ 0xbc78810, __VMLINUX_SYMBOL_STR(mptscsih_slave_configure) },
	{ 0x264df3fe, __VMLINUX_SYMBOL_STR(sas_read_port_mode_page) },
	{ 0xea10212a, __VMLINUX_SYMBOL_STR(int_to_scsilun) },
	{ 0x37befc70, __VMLINUX_SYMBOL_STR(jiffies_to_msecs) },
	{ 0x24d0c29b, __VMLINUX_SYMBOL_STR(mptscsih_taskmgmt_response_code) },
	{ 0x3d654fbe, __VMLINUX_SYMBOL_STR(scsi_device_set_state) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xc238544f, __VMLINUX_SYMBOL_STR(starget_for_each_device) },
	{ 0x1b85919e, __VMLINUX_SYMBOL_STR(mpt_clear_taskmgmt_in_progress_flag) },
	{ 0x8e6c0401, __VMLINUX_SYMBOL_STR(mpt_put_msg_frame_hi_pri) },
	{ 0x9b0c3b48, __VMLINUX_SYMBOL_STR(mpt_set_taskmgmt_in_progress_flag) },
	{ 0x411d9f96, __VMLINUX_SYMBOL_STR(device_reprobe) },
	{ 0xe9fa6851, __VMLINUX_SYMBOL_STR(sas_port_delete) },
	{ 0xb3889d24, __VMLINUX_SYMBOL_STR(mptscsih_ioc_reset) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x54efb5d6, __VMLINUX_SYMBOL_STR(cpu_number) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x7ab80e45, __VMLINUX_SYMBOL_STR(mptscsih_remove) },
	{ 0x5163212c, __VMLINUX_SYMBOL_STR(sas_remove_host) },
	{ 0x409e2dd, __VMLINUX_SYMBOL_STR(mpt_detach) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x416f25b6, __VMLINUX_SYMBOL_STR(cancel_delayed_work) },
	{ 0x4c4fef19, __VMLINUX_SYMBOL_STR(kernel_stack) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0xb088e9e8, __VMLINUX_SYMBOL_STR(mpt_reset_register) },
	{ 0xdac9968f, __VMLINUX_SYMBOL_STR(mpt_event_register) },
	{ 0x24958def, __VMLINUX_SYMBOL_STR(mptscsih_scandv_complete) },
	{ 0x51cbe003, __VMLINUX_SYMBOL_STR(mptscsih_taskmgmt_complete) },
	{ 0x83f7cc6c, __VMLINUX_SYMBOL_STR(mpt_register) },
	{ 0x81c06abd, __VMLINUX_SYMBOL_STR(mptscsih_io_done) },
	{ 0x67540634, __VMLINUX_SYMBOL_STR(sas_attach_transport) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0xdb0b13cf, __VMLINUX_SYMBOL_STR(sas_expander_alloc) },
	{ 0xa494be9b, __VMLINUX_SYMBOL_STR(sas_port_mark_backlink) },
	{ 0x2306b08c, __VMLINUX_SYMBOL_STR(scsi_is_sas_rphy) },
	{ 0x97454b0f, __VMLINUX_SYMBOL_STR(sas_port_add_phy) },
	{ 0x30dfeeac, __VMLINUX_SYMBOL_STR(sas_port_add) },
	{ 0xe08e6e59, __VMLINUX_SYMBOL_STR(sas_port_alloc_num) },
	{ 0x9192202e, __VMLINUX_SYMBOL_STR(sas_phy_free) },
	{ 0x9842830a, __VMLINUX_SYMBOL_STR(sas_phy_add) },
	{ 0x68493633, __VMLINUX_SYMBOL_STR(sas_phy_alloc) },
	{ 0x816d6df, __VMLINUX_SYMBOL_STR(mpt_Soft_Hard_ResetHandler) },
	{ 0xd1960438, __VMLINUX_SYMBOL_STR(mpt_free_msg_frame) },
	{ 0x53f6ffbc, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0xb791135, __VMLINUX_SYMBOL_STR(mpt_put_msg_frame) },
	{ 0x93937fa4, __VMLINUX_SYMBOL_STR(mpt_get_msg_frame) },
	{ 0x8ba98207, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x3400c75c, __VMLINUX_SYMBOL_STR(mpt_raid_phys_disk_pg0) },
	{ 0xda39ca4e, __VMLINUX_SYMBOL_STR(sas_rphy_free) },
	{ 0xc87845aa, __VMLINUX_SYMBOL_STR(sas_rphy_add) },
	{ 0x58482f37, __VMLINUX_SYMBOL_STR(sas_end_device_alloc) },
	{ 0xd381b609, __VMLINUX_SYMBOL_STR(sas_port_delete_phy) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9cbeb511, __VMLINUX_SYMBOL_STR(mptscsih_qcmd) },
	{ 0x491e6a54, __VMLINUX_SYMBOL_STR(scsi_print_command) },
	{ 0x6c137fc1, __VMLINUX_SYMBOL_STR(scsi_cmd_get_serial) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x9bffd2dd, __VMLINUX_SYMBOL_STR(__scsi_iterate_devices) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0xc83e34bd, __VMLINUX_SYMBOL_STR(mpt_config) },
	{ 0xc563212b, __VMLINUX_SYMBOL_STR(mptscsih_raid_id_to_num) },
	{ 0xb5a35d57, __VMLINUX_SYMBOL_STR(mptscsih_is_phys_disk) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x2162100b, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0x36caf629, __VMLINUX_SYMBOL_STR(scsi_is_host_device) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xc47c22e8, __VMLINUX_SYMBOL_STR(mpt_deregister) },
	{ 0x4526289b, __VMLINUX_SYMBOL_STR(mpt_event_deregister) },
	{ 0xd9a92a75, __VMLINUX_SYMBOL_STR(mpt_reset_deregister) },
	{ 0xfced0600, __VMLINUX_SYMBOL_STR(sas_release_transport) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mptscsih,scsi_mod,mptbase,scsi_transport_sas";

MODULE_ALIAS("pci:v00001000d00000050sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000054sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000056sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000058sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000062sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000059sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "44F60444534120E8825C602");
