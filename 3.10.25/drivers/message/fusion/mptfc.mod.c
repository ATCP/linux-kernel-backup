#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x46884346, __VMLINUX_SYMBOL_STR(mptscsih_host_attrs) },
	{ 0xe7cebeac, __VMLINUX_SYMBOL_STR(mptscsih_show_info) },
	{ 0x5183dff5, __VMLINUX_SYMBOL_STR(mptscsih_bios_param) },
	{ 0xeb17aa3b, __VMLINUX_SYMBOL_STR(mptscsih_change_queue_depth) },
	{ 0x448cc567, __VMLINUX_SYMBOL_STR(mptscsih_slave_destroy) },
	{ 0xbc78810, __VMLINUX_SYMBOL_STR(mptscsih_slave_configure) },
	{ 0xc9407759, __VMLINUX_SYMBOL_STR(mptscsih_info) },
	{ 0x4d5d663d, __VMLINUX_SYMBOL_STR(mptscsih_shutdown) },
	{ 0x30394567, __VMLINUX_SYMBOL_STR(mptscsih_resume) },
	{ 0x694c50e2, __VMLINUX_SYMBOL_STR(mptscsih_suspend) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xe3a53f4c, __VMLINUX_SYMBOL_STR(sort) },
	{ 0x9c8a18c9, __VMLINUX_SYMBOL_STR(mptscsih_event_process) },
	{ 0xb3889d24, __VMLINUX_SYMBOL_STR(mptscsih_ioc_reset) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0xb088e9e8, __VMLINUX_SYMBOL_STR(mpt_reset_register) },
	{ 0xdac9968f, __VMLINUX_SYMBOL_STR(mpt_event_register) },
	{ 0x24958def, __VMLINUX_SYMBOL_STR(mptscsih_scandv_complete) },
	{ 0x51cbe003, __VMLINUX_SYMBOL_STR(mptscsih_taskmgmt_complete) },
	{ 0x83f7cc6c, __VMLINUX_SYMBOL_STR(mpt_register) },
	{ 0x81c06abd, __VMLINUX_SYMBOL_STR(mptscsih_io_done) },
	{ 0xf685586f, __VMLINUX_SYMBOL_STR(fc_attach_transport) },
	{ 0xc19be69a, __VMLINUX_SYMBOL_STR(fc_remote_port_rolechg) },
	{ 0x6e2f6e82, __VMLINUX_SYMBOL_STR(fc_remote_port_add) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0x9f365221, __VMLINUX_SYMBOL_STR(fc_remote_port_delete) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x51973243, __VMLINUX_SYMBOL_STR(scsi_add_host_with_dma) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xaddb8893, __VMLINUX_SYMBOL_STR(scsi_host_alloc) },
	{ 0x3de027e4, __VMLINUX_SYMBOL_STR(mpt_attach) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xc83e34bd, __VMLINUX_SYMBOL_STR(mpt_config) },
	{ 0x9cbeb511, __VMLINUX_SYMBOL_STR(mptscsih_qcmd) },
	{ 0x6c137fc1, __VMLINUX_SYMBOL_STR(scsi_cmd_get_serial) },
	{ 0xf92d9b83, __VMLINUX_SYMBOL_STR(mptscsih_abort) },
	{ 0x660e1105, __VMLINUX_SYMBOL_STR(mptscsih_dev_reset) },
	{ 0xa4e54add, __VMLINUX_SYMBOL_STR(mptscsih_bus_reset) },
	{ 0xd68e9809, __VMLINUX_SYMBOL_STR(mptscsih_host_reset) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x5916b44a, __VMLINUX_SYMBOL_STR(scsi_is_fc_rport) },
	{ 0x7ab80e45, __VMLINUX_SYMBOL_STR(mptscsih_remove) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x3e5d856, __VMLINUX_SYMBOL_STR(fc_remove_host) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xc47c22e8, __VMLINUX_SYMBOL_STR(mpt_deregister) },
	{ 0x4526289b, __VMLINUX_SYMBOL_STR(mpt_event_deregister) },
	{ 0xd9a92a75, __VMLINUX_SYMBOL_STR(mpt_reset_deregister) },
	{ 0xb642b041, __VMLINUX_SYMBOL_STR(fc_release_transport) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mptscsih,mptbase,scsi_transport_fc,scsi_mod";

MODULE_ALIAS("pci:v00001000d00000621sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000624sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000622sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000628sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000626sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000642sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000640sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001000d00000646sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001657d00000646sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "F4A7A3DEF554D8E0CFFDAF7");
