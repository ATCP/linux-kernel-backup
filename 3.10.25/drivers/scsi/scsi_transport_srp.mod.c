#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc34599ee, __VMLINUX_SYMBOL_STR(transport_class_register) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x4d9023ce, __VMLINUX_SYMBOL_STR(transport_destroy_device) },
	{ 0x8cbca513, __VMLINUX_SYMBOL_STR(attribute_container_unregister) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x36caf629, __VMLINUX_SYMBOL_STR(scsi_is_host_device) },
	{ 0x620ec38d, __VMLINUX_SYMBOL_STR(device_del) },
	{ 0xd1a07b53, __VMLINUX_SYMBOL_STR(transport_add_device) },
	{ 0xf49d27b6, __VMLINUX_SYMBOL_STR(transport_configure_device) },
	{ 0x3d8da4a0, __VMLINUX_SYMBOL_STR(attribute_container_register) },
	{ 0x20d3df20, __VMLINUX_SYMBOL_STR(device_add) },
	{ 0x5a3b2f59, __VMLINUX_SYMBOL_STR(transport_class_unregister) },
	{ 0x17a54f1c, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0x2aad9969, __VMLINUX_SYMBOL_STR(transport_setup_device) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xb0415876, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0xf498dc8c, __VMLINUX_SYMBOL_STR(device_for_each_child) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xb28c531c, __VMLINUX_SYMBOL_STR(device_initialize) },
	{ 0x75da42c8, __VMLINUX_SYMBOL_STR(transport_remove_device) },
	{ 0x291f89b7, __VMLINUX_SYMBOL_STR(dev_set_name) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=scsi_mod";


MODULE_INFO(srcversion, "8899FC595D8A4D28A5DD22B");
