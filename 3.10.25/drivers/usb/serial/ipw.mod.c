#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x880d7eff, __VMLINUX_SYMBOL_STR(usb_wwan_write) },
	{ 0x695efd14, __VMLINUX_SYMBOL_STR(usb_wwan_port_remove) },
	{ 0x16f90fd, __VMLINUX_SYMBOL_STR(usb_wwan_port_probe) },
	{ 0xb63bf762, __VMLINUX_SYMBOL_STR(usb_serial_register_drivers) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x501e6ef2, __VMLINUX_SYMBOL_STR(usb_wwan_open) },
	{ 0xdcb2aae1, __VMLINUX_SYMBOL_STR(usb_clear_halt) },
	{ 0x448eac3e, __VMLINUX_SYMBOL_STR(kmemdup) },
	{ 0x3983c86b, __VMLINUX_SYMBOL_STR(usb_wwan_close) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xf0119601, __VMLINUX_SYMBOL_STR(usb_control_msg) },
	{ 0xaee501bc, __VMLINUX_SYMBOL_STR(usb_serial_deregister_drivers) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usb_wwan,usbserial";

MODULE_ALIAS("usb:v0BC3p0001d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "57E50700F6E5139B164E9DA");
