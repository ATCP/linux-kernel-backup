#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc69dd24f, __VMLINUX_SYMBOL_STR(usb_hcd_pci_pm_ops) },
	{ 0x3ba0cccf, __VMLINUX_SYMBOL_STR(usb_hcd_pci_remove) },
	{ 0x4c3e4755, __VMLINUX_SYMBOL_STR(usb_hcd_pci_probe) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x19a304ba, __VMLINUX_SYMBOL_STR(usb_disabled) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xc17515d7, __VMLINUX_SYMBOL_STR(usb_hcds_loaded) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0xd4ed8b1a, __VMLINUX_SYMBOL_STR(usb_hc_died) },
	{ 0xcb60aa85, __VMLINUX_SYMBOL_STR(uhci_reset_hc) },
	{ 0x53171f92, __VMLINUX_SYMBOL_STR(uhci_check_and_reset_hc) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x81e6b37f, __VMLINUX_SYMBOL_STR(dmi_get_system_info) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x5d65fc2e, __VMLINUX_SYMBOL_STR(debugfs_remove) },
	{ 0x72a3ccf8, __VMLINUX_SYMBOL_STR(dma_pool_create) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x87f70505, __VMLINUX_SYMBOL_STR(pci_bus_write_config_byte) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0x5a7d3ae7, __VMLINUX_SYMBOL_STR(usb_hcd_poll_rh_status) },
	{ 0xa13c2925, __VMLINUX_SYMBOL_STR(usb_root_hub_lost_power) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xb5aa7165, __VMLINUX_SYMBOL_STR(dma_pool_destroy) },
	{ 0x6f0036d9, __VMLINUX_SYMBOL_STR(del_timer_sync) },
	{ 0xe523ad75, __VMLINUX_SYMBOL_STR(synchronize_irq) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xec9d06aa, __VMLINUX_SYMBOL_STR(usb_hcd_link_urb_to_ep) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x7e64181d, __VMLINUX_SYMBOL_STR(usb_calc_bus_time) },
	{ 0x771cf835, __VMLINUX_SYMBOL_STR(dma_pool_alloc) },
	{ 0x8170740, __VMLINUX_SYMBOL_STR(usb_hcd_check_unlink_urb) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xc9268b74, __VMLINUX_SYMBOL_STR(usb_hcd_resume_root_hub) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xb7296fff, __VMLINUX_SYMBOL_STR(usb_hcd_start_port_resume) },
	{ 0xec8dea2c, __VMLINUX_SYMBOL_STR(usb_hcd_end_port_resume) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xe82af09c, __VMLINUX_SYMBOL_STR(usb_hcd_giveback_urb) },
	{ 0x75c54af2, __VMLINUX_SYMBOL_STR(usb_hcd_unlink_urb_from_ep) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x2a37d074, __VMLINUX_SYMBOL_STR(dma_pool_free) },
	{ 0x1e047854, __VMLINUX_SYMBOL_STR(warn_slowpath_fmt) },
	{ 0x9480348c, __VMLINUX_SYMBOL_STR(dev_driver_string) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xc8fd727e, __VMLINUX_SYMBOL_STR(mod_timer) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v*d*sv*sd*bc0Csc03i00*");

MODULE_INFO(srcversion, "92AE1B2928588BC34C3E8D5");
