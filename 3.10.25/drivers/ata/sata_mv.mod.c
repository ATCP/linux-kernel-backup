#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x116eeefd, __VMLINUX_SYMBOL_STR(ata_bmdma_port_ops) },
	{ 0xdce0983e, __VMLINUX_SYMBOL_STR(ata_sff_port_ops) },
	{ 0x906ed660, __VMLINUX_SYMBOL_STR(ata_common_sdev_attrs) },
	{ 0x63205de1, __VMLINUX_SYMBOL_STR(ata_scsi_unlock_native_capacity) },
	{ 0x32d436c7, __VMLINUX_SYMBOL_STR(ata_std_bios_param) },
	{ 0xd32fe193, __VMLINUX_SYMBOL_STR(ata_scsi_change_queue_depth) },
	{ 0x94a68723, __VMLINUX_SYMBOL_STR(ata_scsi_slave_destroy) },
	{ 0x69e9a2cb, __VMLINUX_SYMBOL_STR(ata_scsi_slave_config) },
	{ 0xcc94722f, __VMLINUX_SYMBOL_STR(ata_scsi_queuecmd) },
	{ 0x39a0cbce, __VMLINUX_SYMBOL_STR(ata_scsi_ioctl) },
	{ 0x4393d5d4, __VMLINUX_SYMBOL_STR(ata_pci_device_suspend) },
	{ 0x5f1d5630, __VMLINUX_SYMBOL_STR(ata_pci_remove_one) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x3c8746c6, __VMLINUX_SYMBOL_STR(platform_driver_register) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x29d75eb3, __VMLINUX_SYMBOL_STR(pci_try_set_mwi) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0xe722ff65, __VMLINUX_SYMBOL_STR(pci_bus_read_config_byte) },
	{ 0x98616114, __VMLINUX_SYMBOL_STR(pci_enable_msi_block) },
	{ 0x77f230ad, __VMLINUX_SYMBOL_STR(ata_port_pbar_desc) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0xffde746c, __VMLINUX_SYMBOL_STR(pcim_iomap_table) },
	{ 0xa6cec0d, __VMLINUX_SYMBOL_STR(pcim_pin_device) },
	{ 0x5eb4963f, __VMLINUX_SYMBOL_STR(pcim_iomap_regions) },
	{ 0xeb8d93d2, __VMLINUX_SYMBOL_STR(pcim_enable_device) },
	{ 0xb2d449ca, __VMLINUX_SYMBOL_STR(ata_pci_device_do_resume) },
	{ 0x7e7ed358, __VMLINUX_SYMBOL_STR(ata_bmdma_port_intr) },
	{ 0x11b4af98, __VMLINUX_SYMBOL_STR(ata_port_abort) },
	{ 0x547aa658, __VMLINUX_SYMBOL_STR(sata_async_notification) },
	{ 0xc54a871f, __VMLINUX_SYMBOL_STR(sata_scr_write_flush) },
	{ 0x8102b6e1, __VMLINUX_SYMBOL_STR(ata_link_offline) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x74c134b9, __VMLINUX_SYMBOL_STR(__sw_hweight32) },
	{ 0xcab1cbb5, __VMLINUX_SYMBOL_STR(ata_link_abort) },
	{ 0x39c4f954, __VMLINUX_SYMBOL_STR(ata_ehi_push_desc) },
	{ 0xd683565e, __VMLINUX_SYMBOL_STR(ata_ehi_clear_desc) },
	{ 0xc41e162f, __VMLINUX_SYMBOL_STR(ata_qc_complete_multiple) },
	{ 0x33374b1f, __VMLINUX_SYMBOL_STR(ata_host_activate) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x8a257f50, __VMLINUX_SYMBOL_STR(devm_ioremap) },
	{ 0xe982fc75, __VMLINUX_SYMBOL_STR(ata_host_alloc_pinfo) },
	{ 0x8a2b33a, __VMLINUX_SYMBOL_STR(platform_get_irq) },
	{ 0x86bad130, __VMLINUX_SYMBOL_STR(platform_get_resource) },
	{ 0x7f2a05ca, __VMLINUX_SYMBOL_STR(ata_print_version) },
	{ 0xd67a109e, __VMLINUX_SYMBOL_STR(dmam_pool_create) },
	{ 0xc18f4003, __VMLINUX_SYMBOL_STR(ata_bmdma_qc_issue) },
	{ 0xd726607a, __VMLINUX_SYMBOL_STR(ata_sff_queue_pio_task) },
	{ 0x8b752ac1, __VMLINUX_SYMBOL_STR(ata_tf_to_fis) },
	{ 0xbc2fb93f, __VMLINUX_SYMBOL_STR(ata_link_printk) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xc33e4899, __VMLINUX_SYMBOL_STR(sata_scr_read) },
	{ 0xebaeb744, __VMLINUX_SYMBOL_STR(sata_link_hardreset) },
	{ 0x4fdc945d, __VMLINUX_SYMBOL_STR(sata_deb_timing_normal) },
	{ 0xcccfb2fa, __VMLINUX_SYMBOL_STR(sata_deb_timing_hotplug) },
	{ 0x771cf835, __VMLINUX_SYMBOL_STR(dma_pool_alloc) },
	{ 0xa9bd7b88, __VMLINUX_SYMBOL_STR(devm_kzalloc) },
	{ 0x2a37d074, __VMLINUX_SYMBOL_STR(dma_pool_free) },
	{ 0xa3ed90ae, __VMLINUX_SYMBOL_STR(ata_port_printk) },
	{ 0xf7aaf36f, __VMLINUX_SYMBOL_STR(ata_dev_printk) },
	{ 0x66a64e07, __VMLINUX_SYMBOL_STR(ata_sff_softreset) },
	{ 0x3233c596, __VMLINUX_SYMBOL_STR(sata_std_hardreset) },
	{ 0x736922c5, __VMLINUX_SYMBOL_STR(sata_pmp_error_handler) },
	{ 0x255686e9, __VMLINUX_SYMBOL_STR(ata_port_freeze) },
	{ 0xdd76745b, __VMLINUX_SYMBOL_STR(ata_eh_analyze_ncq_error) },
	{ 0x7c3c6715, __VMLINUX_SYMBOL_STR(ata_sff_dma_pause) },
	{ 0xf10de535, __VMLINUX_SYMBOL_STR(ioread8) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x680dfe41, __VMLINUX_SYMBOL_STR(ata_host_detach) },
	{ 0x92fbd5e2, __VMLINUX_SYMBOL_STR(ata_host_suspend) },
	{ 0x96affe9b, __VMLINUX_SYMBOL_STR(ata_host_resume) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x3f34327f, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libata";

MODULE_ALIAS("pci:v000011ABd00005040sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00005041sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00005080sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00005081sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001103d00001720sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001103d00001740sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001103d00001742sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006040sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006041sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006042sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006080sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006081sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00009005d00000241sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00009005d00000243sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00007042sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001103d00002300sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001103d00002310sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "76EEB3B2B1BF29F11EF932F");
