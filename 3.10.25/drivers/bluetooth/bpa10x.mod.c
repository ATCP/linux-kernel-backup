#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb3d01079, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0x356b3416, __VMLINUX_SYMBOL_STR(hci_recv_frame) },
	{ 0x45f4292a, __VMLINUX_SYMBOL_STR(skb_put) },
	{ 0x5d2dc510, __VMLINUX_SYMBOL_STR(__alloc_skb) },
	{ 0x8b68ce9f, __VMLINUX_SYMBOL_STR(usb_kill_anchored_urbs) },
	{ 0xc2cea7ff, __VMLINUX_SYMBOL_STR(usb_unanchor_urb) },
	{ 0xb61a0c3b, __VMLINUX_SYMBOL_STR(bt_err) },
	{ 0xf503b80a, __VMLINUX_SYMBOL_STR(usb_submit_urb) },
	{ 0x7d0aa01f, __VMLINUX_SYMBOL_STR(usb_anchor_urb) },
	{ 0x32eaa242, __VMLINUX_SYMBOL_STR(usb_free_urb) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xe6961bf3, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0xfc44da0f, __VMLINUX_SYMBOL_STR(usb_alloc_urb) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x4a72ab50, __VMLINUX_SYMBOL_STR(hci_register_dev) },
	{ 0xe549262a, __VMLINUX_SYMBOL_STR(hci_alloc_dev) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xa9bd7b88, __VMLINUX_SYMBOL_STR(devm_kzalloc) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x4f552bd, __VMLINUX_SYMBOL_STR(hci_free_dev) },
	{ 0x9beddc3f, __VMLINUX_SYMBOL_STR(hci_unregister_dev) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xdf01d54, __VMLINUX_SYMBOL_STR(usb_deregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=bluetooth";

MODULE_ALIAS("usb:v08FDp0002d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "D5AEEC9C8BDFABBB00DE517");
