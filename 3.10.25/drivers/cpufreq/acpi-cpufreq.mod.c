#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1d8c2cae, __VMLINUX_SYMBOL_STR(cpufreq_freq_attr_scaling_available_freqs) },
	{ 0xbc2031de, __VMLINUX_SYMBOL_STR(acpi_processor_get_bios_limit) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x111bc7a0, __VMLINUX_SYMBOL_STR(sysfs_create_file) },
	{ 0x8b43159b, __VMLINUX_SYMBOL_STR(register_cpu_notifier) },
	{ 0xb1cfad22, __VMLINUX_SYMBOL_STR(rdmsr_on_cpu) },
	{ 0x3d7c1ed7, __VMLINUX_SYMBOL_STR(msrs_alloc) },
	{ 0x20694570, __VMLINUX_SYMBOL_STR(cpufreq_register_driver) },
	{ 0xd4c7566f, __VMLINUX_SYMBOL_STR(acpi_processor_preregister_performance) },
	{ 0x618911fc, __VMLINUX_SYMBOL_STR(numa_node) },
	{ 0x9784482b, __VMLINUX_SYMBOL_STR(zalloc_cpumask_var_node) },
	{ 0x949f7342, __VMLINUX_SYMBOL_STR(__alloc_percpu) },
	{ 0x1a45cb6c, __VMLINUX_SYMBOL_STR(acpi_disabled) },
	{ 0xa4a10505, __VMLINUX_SYMBOL_STR(cpufreq_unregister_driver) },
	{ 0xcf6cfe1f, __VMLINUX_SYMBOL_STR(msrs_free) },
	{ 0x1fe9f800, __VMLINUX_SYMBOL_STR(unregister_cpu_notifier) },
	{ 0x52b1c573, __VMLINUX_SYMBOL_STR(sysfs_remove_file) },
	{ 0x2fc18e62, __VMLINUX_SYMBOL_STR(cpufreq_global_kobject) },
	{ 0xc9ec4e21, __VMLINUX_SYMBOL_STR(free_percpu) },
	{ 0xe8367c2d, __VMLINUX_SYMBOL_STR(free_cpumask_var) },
	{ 0x76a495c1, __VMLINUX_SYMBOL_STR(cpu_possible_mask) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xd7d79132, __VMLINUX_SYMBOL_STR(put_online_cpus) },
	{ 0x950ffff2, __VMLINUX_SYMBOL_STR(cpu_online_mask) },
	{ 0x3efb35c9, __VMLINUX_SYMBOL_STR(get_online_cpus) },
	{ 0x60ea2d6, __VMLINUX_SYMBOL_STR(kstrtoull) },
	{ 0x5ebe3824, __VMLINUX_SYMBOL_STR(wrmsr_on_cpus) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0xcabe18d9, __VMLINUX_SYMBOL_STR(rdmsr_on_cpus) },
	{ 0x706b3a33, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_get_attr) },
	{ 0xe8bd6225, __VMLINUX_SYMBOL_STR(cpufreq_get_measured_perf) },
	{ 0x774f4c66, __VMLINUX_SYMBOL_STR(acpi_processor_notify_smm) },
	{ 0xd6b33026, __VMLINUX_SYMBOL_STR(cpu_khz) },
	{ 0xadb8574b, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_cpuinfo) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xd3caf86, __VMLINUX_SYMBOL_STR(cpu_core_map) },
	{ 0x4cbbd171, __VMLINUX_SYMBOL_STR(__bitmap_weight) },
	{ 0xd4835ef8, __VMLINUX_SYMBOL_STR(dmi_check_system) },
	{ 0x574a1d88, __VMLINUX_SYMBOL_STR(acpi_processor_register_performance) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x846f59a, __VMLINUX_SYMBOL_STR(cpu_info) },
	{ 0xafdf88c5, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_verify) },
	{ 0xbdaf5b07, __VMLINUX_SYMBOL_STR(acpi_os_read_port) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xa32d0dc9, __VMLINUX_SYMBOL_STR(smp_call_function_many) },
	{ 0x54efb5d6, __VMLINUX_SYMBOL_STR(cpu_number) },
	{ 0x3400c54f, __VMLINUX_SYMBOL_STR(cpufreq_notify_transition) },
	{ 0xa9231bca, __VMLINUX_SYMBOL_STR(cpu_bit_bitmap) },
	{ 0xf16add4, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_target) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xa3d0b1e2, __VMLINUX_SYMBOL_STR(smp_call_function_any) },
	{ 0xb352177e, __VMLINUX_SYMBOL_STR(find_first_bit) },
	{ 0x6d27ef64, __VMLINUX_SYMBOL_STR(__bitmap_empty) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0x7d94f746, __VMLINUX_SYMBOL_STR(acpi_os_write_port) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xdcdafbed, __VMLINUX_SYMBOL_STR(acpi_processor_unregister_performance) },
	{ 0x7ae1ae8e, __VMLINUX_SYMBOL_STR(cpufreq_frequency_table_put_attr) },
	{ 0x32047ad5, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0xc715d9e0, __VMLINUX_SYMBOL_STR(boot_cpu_data) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=freq_table,mperf";

MODULE_ALIAS("x86cpu:vendor:*:family:*:model:*:feature:*0016*");
MODULE_ALIAS("x86cpu:vendor:*:family:*:model:*:feature:*00E8*");

MODULE_INFO(srcversion, "C679FDC8F710810C86EC0B6");
