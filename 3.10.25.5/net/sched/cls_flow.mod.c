#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x6954e5cf, __VMLINUX_SYMBOL_STR(register_tcf_proto_ops) },
	{ 0x51e3e5a5, __VMLINUX_SYMBOL_STR(tcf_action_exec) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x15a56777, __VMLINUX_SYMBOL_STR(__skb_get_rxhash) },
	{ 0xd0561bd6, __VMLINUX_SYMBOL_STR(skb_flow_dissect) },
	{ 0x8e8b6732, __VMLINUX_SYMBOL_STR(__tcf_em_tree_match) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0x21523024, __VMLINUX_SYMBOL_STR(tcf_exts_change) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x98824a1c, __VMLINUX_SYMBOL_STR(tcf_em_tree_validate) },
	{ 0x9823e147, __VMLINUX_SYMBOL_STR(tcf_exts_validate) },
	{ 0xe6dd04af, __VMLINUX_SYMBOL_STR(init_user_ns) },
	{ 0x74c134b9, __VMLINUX_SYMBOL_STR(__sw_hweight32) },
	{ 0x4f391d0e, __VMLINUX_SYMBOL_STR(nla_parse) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xc8fd727e, __VMLINUX_SYMBOL_STR(mod_timer) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x79aa04a2, __VMLINUX_SYMBOL_STR(get_random_bytes) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0x6b2dc060, __VMLINUX_SYMBOL_STR(dump_stack) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x85670f1d, __VMLINUX_SYMBOL_STR(rtnl_is_locked) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x47d16ce0, __VMLINUX_SYMBOL_STR(tcf_em_tree_destroy) },
	{ 0x25722da3, __VMLINUX_SYMBOL_STR(tcf_exts_destroy) },
	{ 0x6f0036d9, __VMLINUX_SYMBOL_STR(del_timer_sync) },
	{ 0xf00f8562, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0x884df0f6, __VMLINUX_SYMBOL_STR(tcf_exts_dump_stats) },
	{ 0x6bd89930, __VMLINUX_SYMBOL_STR(tcf_em_tree_dump) },
	{ 0x1337f91d, __VMLINUX_SYMBOL_STR(tcf_exts_dump) },
	{ 0xfa0f6d09, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0x3864f692, __VMLINUX_SYMBOL_STR(unregister_tcf_proto_ops) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "71027B3313AB0F4187C4FE9");
