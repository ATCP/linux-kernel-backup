#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd187d88c, __VMLINUX_SYMBOL_STR(xfrm6_find_1stfragopt) },
	{ 0x13aa0ad8, __VMLINUX_SYMBOL_STR(ipcomp_output) },
	{ 0x794b0499, __VMLINUX_SYMBOL_STR(ipcomp_input) },
	{ 0xef7fb819, __VMLINUX_SYMBOL_STR(ipcomp_destroy) },
	{ 0xff75b077, __VMLINUX_SYMBOL_STR(xfrm6_rcv) },
	{ 0x68dcfdc1, __VMLINUX_SYMBOL_STR(inet6_add_protocol) },
	{ 0xd7c99d0c, __VMLINUX_SYMBOL_STR(xfrm_register_type) },
	{ 0xcf9b4b2b, __VMLINUX_SYMBOL_STR(ip6_update_pmtu) },
	{ 0x70b05fec, __VMLINUX_SYMBOL_STR(ip6_redirect) },
	{ 0x3b0e3032, __VMLINUX_SYMBOL_STR(__xfrm_state_destroy) },
	{ 0xe19a7788, __VMLINUX_SYMBOL_STR(xfrm_state_insert) },
	{ 0xff0aeba4, __VMLINUX_SYMBOL_STR(xfrm_init_state) },
	{ 0x62258174, __VMLINUX_SYMBOL_STR(xfrm6_tunnel_alloc_spi) },
	{ 0x174a9e27, __VMLINUX_SYMBOL_STR(xfrm_state_alloc) },
	{ 0xd28d488, __VMLINUX_SYMBOL_STR(xfrm_state_lookup) },
	{ 0xfcfde395, __VMLINUX_SYMBOL_STR(xfrm6_tunnel_spi_lookup) },
	{ 0x3dfd709c, __VMLINUX_SYMBOL_STR(ipcomp_init_state) },
	{ 0xcca6a892, __VMLINUX_SYMBOL_STR(xfrm_unregister_type) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x9b4181a3, __VMLINUX_SYMBOL_STR(inet6_del_protocol) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ipv6,xfrm_ipcomp,xfrm6_tunnel";


MODULE_INFO(srcversion, "1BEB92960842A1BF84ABAFE");
