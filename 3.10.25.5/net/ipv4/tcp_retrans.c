/*
 * Pluggable TCP retransmission algorithm support and 
 * PRR retransmission algorithm.
 * Inspired by the ideas of Pluggable TCP congestion control support.
 *
 * Author: Perth Charles <zhongbincharles@gmail.com> @2014
 */

#define pr_fmt(fmt) "TCP: " fmt

#include <linux/module.h>
#include <linux/types.h>
#include <linux/list.h>
#include <net/tcp.h>

static DEFINE_SPINLOCK(tcp_retrans_list_lock);
static LIST_HEAD(tcp_retrans_list);

/* Support for Robust Rate Reduction(RRR) and Vegas */
int sysctl_tcp_vegas_alpha = 2;
int sysctl_tcp_vegas_beta  = 4;
int sysctl_tcp_congestion_threshold = 3;
/* wanwenkai */
int sysctl_tcp_temp_threshold = 3;
int sysctl_tcp_queue_threshold = 200;
/* end */
EXPORT_SYMBOL(sysctl_tcp_vegas_alpha);
EXPORT_SYMBOL(sysctl_tcp_vegas_beta);
EXPORT_SYMBOL(sysctl_tcp_congestion_threshold);
/* wanwenkai */
EXPORT_SYMBOL(sysctl_tcp_temp_threshold);
EXPORT_SYMBOL(sysctl_tcp_queue_threshold);
/* end */

/* find a retransmission algorithm */
static struct tcp_retrans_ops *tcp_retrans_find(const char *name)
{
    struct tcp_retrans_ops *rexmit;

    list_for_each_entry_rcu(rexmit, &tcp_retrans_list, list) {
        if (strcmp(rexmit->name, name) == 0) {
            return rexmit;
        }
    }

    return NULL;
}

/* register retransmission algorithm */
int tcp_register_retransmission_algorithm(struct tcp_retrans_ops *rexmit)
{
    int ret = 0;

    /* these three componets must be implemented */
    if (!rexmit->init_cwnd_reduction || !rexmit->cwnd_reduction 
       || !rexmit->end_cwnd_reduction) {
        pr_err("%s does not implement required ops\n", rexmit->name);
        return -EINVAL;
    }

    spin_lock(&tcp_retrans_list_lock);
    if (tcp_retrans_find(rexmit->name)) {
        pr_notice("%s already registered\n", rexmit->name);
        ret = -EEXIST;
    } else {
        list_add_tail_rcu(&rexmit->list, &tcp_retrans_list);
        pr_info("%s registered\n", rexmit->name);
    }
    spin_unlock(&tcp_retrans_list_lock);

    return ret;
}
EXPORT_SYMBOL_GPL(tcp_register_retransmission_algorithm);

/* unregister retransmission algorithm */
void tcp_unregister_retransmission_algorithm(struct tcp_retrans_ops *rexmit)
{
    spin_lock(&tcp_retrans_list_lock);
    list_del_rcu(&rexmit->list);
    spin_unlock(&tcp_retrans_list_lock);
}
EXPORT_SYMBOL_GPL(tcp_unregister_retransmission_algorithm);

/* init retransmission_algorithm for A tcp connection */
void tcp_init_retransmission_algorithm(struct sock *sk)
{
    struct inet_connection_sock *icsk = inet_csk(sk);
    struct tcp_retrans_ops *rexmit;

    rcu_read_lock();
    list_for_each_entry_rcu(rexmit, &tcp_retrans_list, list) {
        if (try_module_get(rexmit->owner)) {
            icsk->icsk_retrans_ops = rexmit;
            break;
        }
    }
    rcu_read_unlock();

    if (!tcp_retrans_find(icsk->icsk_retrans_ops->name)) {
        printk(KERN_ALERT "RETRANS_MODULE: the set of icsk_retrans_ops is WRONG. \n");
    }
//
//    } else {
//        printk(KERN_ALERT "RETRANS_MODULE: the set of icsk_retrans_ops is %s. \n", icsk->icsk_retrans_ops->name);
//    }
}

void tcp_cleanup_retransmission_algorithm(struct sock *sk)
{
    struct inet_connection_sock *icsk = inet_csk(sk);

    module_put(icsk->icsk_retrans_ops->owner);
}

/* 
 * Set default retransmission algorithm 
 * For proc interface
 */
int tcp_set_default_retransmission_algorithm(const char *name)
{
    struct tcp_retrans_ops *rexmit;
    int ret = -ENOENT;

    spin_lock(&tcp_retrans_list_lock);

    rexmit = tcp_retrans_find(name);
    if (rexmit) {
        list_move(&rexmit->list, &tcp_retrans_list);
        ret = 0;
    }

    spin_unlock(&tcp_retrans_list_lock);

    return ret;
}

/* Set default retransmission algorithm at bootup */
static int __init tcp_retransmission_default(void)
{
    return tcp_set_default_retransmission_algorithm(DEFAULT_RETRANS_ALGORITHM);
}
late_initcall(tcp_retransmission_default);


/* 
 * Get available retransmission algorithm info
 * For proc interface
 */
void tcp_get_available_retransmission_algorithm(char *buf, size_t maxlen)
{
    struct tcp_retrans_ops *rexmit;
    size_t offs = 0;

    rcu_read_lock();
    list_for_each_entry_rcu(rexmit, &tcp_retrans_list, list) {
        offs += snprintf(buf + offs, maxlen - offs,
                         "%s%s",
                         offs == 0 ? "" : " ", rexmit->name);
    }
    rcu_read_unlock();
}


/* 
 * Get current default congestion control 
 * For proc interface
 */
void tcp_get_default_retransmission_algorithm(char *name)
{
    struct tcp_retrans_ops *rexmit;
    /* PRR is the default retransmission algorithm and should always be here */
    BUG_ON(list_empty(&tcp_retrans_list));

    rcu_read_lock();
    rexmit = list_entry(tcp_retrans_list.next, struct tcp_retrans_ops, list);
    strncpy(name, rexmit->name, TCP_RETRANS_NAME_MAX);
    rcu_read_unlock();
}

/* 
 * Set retransmission algorithm 
 * For socket interface
 */
int tcp_set_retransmission_algorithm(struct sock *sk, const char *name)
{
    struct inet_connection_sock *icsk = inet_csk(sk);
    struct tcp_retrans_ops *rexmit;
    int err = 0;
    
    rcu_read_lock();
    rexmit = tcp_retrans_find(name);

    if (rexmit == icsk->icsk_retrans_ops) {
        goto out;
    }

    if (!rexmit) {
        err = -ENOENT;
    } else if (!try_module_get(rexmit->owner)) {
        err = -EBUSY;
    } else {
	tcp_cleanup_retransmission_algorithm(sk);
        icsk->icsk_retrans_ops = rexmit;
    }
    
 out:
    rcu_read_unlock();
    return err;
}


/* The PRR algorithm used for cwnd reduction in CWR and Recovery
 * https://datatracker.ietf.org/doc/draft-ietf-tcpm-proportional-rate-reduction/
 * It computes the number of packets to send (sndcnt) based on packets newly
 * delivered:
 *   1) If the packets in flight is larger than ssthresh, PRR spreads the
 *	cwnd reductions across a full RTT.
 *   2) If packets in flight is lower than ssthresh (such as due to excess
 *	losses and/or application stalls), do not perform any further cwnd
 *	reductions, but instead slow start up to ssthresh.
 */
void tcp_prr_init_cwnd_reduction(struct sock *sk, const bool set_ssthresh)
{
    struct tcp_sock *tp = tcp_sk(sk);

    tp->high_seq = tp->snd_nxt;
    tp->tlp_high_seq = 0;
    tp->snd_cwnd_cnt = 0;
    tp->prior_cwnd = tp->snd_cwnd;
    tp->prr_delivered = 0;
    tp->prr_out = 0;
    if (set_ssthresh) {
        tp->snd_ssthresh = inet_csk(sk)->icsk_ca_ops->ssthresh(sk);
    }
    if(tp->ecn_flags & TCP_ECN_OK){
        tp->ecn_flags |= TCP_ECN_QUEUE_CWR;
    }

}
EXPORT_SYMBOL_GPL(tcp_prr_init_cwnd_reduction);

void tcp_prr_cwnd_reduction(struct sock *sk, int newly_acked_sacked,
                            int fast_rexmit)
{
    struct tcp_sock *tp = tcp_sk(sk);
    int sndcnt = 0;
    int delta = tp->snd_ssthresh - tcp_packets_in_flight(tp);

    tp->prr_delivered += newly_acked_sacked;
    if (tcp_packets_in_flight(tp) > tp->snd_ssthresh) {
        u64 dividend = (u64)tp->snd_ssthresh * tp->prr_delivered +
                       tp->prior_cwnd - 1;
        sndcnt = div_u64(dividend, tp->prior_cwnd) - tp->prr_out;
    } else {
        sndcnt = min_t(int, delta,
                       max_t(int, tp->prr_delivered - tp->prr_out,
                             newly_acked_sacked) + 1);
    }

    sndcnt = max(sndcnt, (fast_rexmit ? 1 : 0));
    tp->snd_cwnd = tcp_packets_in_flight(tp) + sndcnt;
}
EXPORT_SYMBOL_GPL(tcp_prr_cwnd_reduction);

void tcp_prr_end_cwnd_reduction(struct sock *sk)
{

    struct tcp_sock *tp = tcp_sk(sk);

    /* Reset cwnd to ssthresh in CWR or Recovery (unless it's undone) */
    if (inet_csk(sk)->icsk_ca_state == TCP_CA_CWR ||
        (tp->undo_marker && tp->snd_ssthresh < TCP_INFINITE_SSTHRESH)) {
        tp->snd_cwnd = tp->snd_ssthresh;
        tp->snd_cwnd_stamp = tcp_time_stamp;
    }
    tcp_ca_event(sk, CA_EVENT_COMPLETE_CWR);

}
EXPORT_SYMBOL_GPL(tcp_prr_end_cwnd_reduction);

struct tcp_retrans_ops tcp_prr = {
    .name                = "PRR",
    .owner               = THIS_MODULE,
    .init_cwnd_reduction = tcp_prr_init_cwnd_reduction,
    .cwnd_reduction      = tcp_prr_cwnd_reduction,
    .end_cwnd_reduction  = tcp_prr_end_cwnd_reduction,
};
