#ifndef TCP_FLOWMSG_H
#define TCP_FLOWMSG_H


/* So far, using define for diff value type. Optimize me when type increase! */
#define FLOW_ADD 1
#define FLOW_DEL 2

#define SET_CWND 11

/* The format of message between host client and controller */
struct flow_msg {
    ktime_t tstamp;
    u64     sock_net;    
    u64     dev_index;   

    union {
        u64     addr_pair;   
        struct {
            __be32 daddr;
            __be32 saddr;
        };
    };
    union {
        u32     port_pair;   
        struct {
            __be16 dport;
            __u16  snum;     // snum = ntohs(sport)
        };
    };
    /* So far, flag and value is limited as fixed length. Optimize me! */
    u32 flag;     // type  of order
    u32 value;    // value of order
};

/* Buffer manager */
struct ccph_buf {
    spinlock_t         lock;
    wait_queue_head_t  wait;
    ktime_t            start;
    
    unsigned long      head, tail;
    struct flow_msg   *buf;    // this buffer is a circular array
};

inline struct flow_msg* ccph_buf_tail(const struct ccph_buf *bufmgr)
{
    return bufmgr->buf + bufmgr->tail;
}

inline struct flow_msg* ccph_buf_head(const struct ccph_buf *bufmgr)
{
    return bufmgr->buf + bufmgr->head;
}

inline void ccph_buf_add(const struct ccph_buf *bufmgr, unsigned int bufsize)
{
    bufmgr->head = (bufmgr->head + 1) & (bufsize - 1);
}

inline void ccph_buf_del(const struct ccph_buf *bufmgr, unsigned int bufsize)
{
     bufmgr->tail = (bufmgr->tail + 1) & (bufsize - 1);
}

inline int ccph_buf_used(const struct ccph_buf *bufmgr, unsigned int bufsize)
{
    return (bufmgr->head - bufmgr->tail) & (bufsize - 1);
}

inline int ccph_buf_avail(const struct ccph_buf *bufmgr, unsigned int bufsize)
{
    return (bufsize - 1) - ccph_buf_used(bufmgr, bufsize);
}

inline void ccph_buf_init(const struct ccph_buf *bufmgr)
{
    spin_lock_bh(&bufmgr->lock);
    bufmgr->head = bufmgr->tail = 0;
    bufmgr->start = ktime_get();
    spin_unlock_bh(&bufmgr->lock);
}

/* convert the binary msg into readable msg */
int flowmsg_readone(const struct ccph_buf *bufmgr, char *tbuf, int n)
{
    const struct flow_msg *cur = bufmgr->buf + bufmgr->tail;
    struct timespec tv 
            = ktime_to_timespec(ktime_sub(cur->tstamp, bufmgr->start));

    return scnprintf(tbuf, n, "%lu.%09lu [s]%pI4:%u [d]%pI4:%u %#llx %lld %u %u\n",
                              (unsigned long) tv.tv_sec,
                              (unsigned long) tv.tv_nsec,
                              &cur->saddr, cur->snum,
                              &cur->daddr, ntohs(cur->dport),
                              cur->sock_net,
                              cur->dev_index,
                              cur->flag,
                              cur->value);
}

#endif
