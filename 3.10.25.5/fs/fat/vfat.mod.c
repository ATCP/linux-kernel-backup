#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x749d68cb, __VMLINUX_SYMBOL_STR(fat_detach) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x405c1144, __VMLINUX_SYMBOL_STR(get_seconds) },
	{ 0x7ae34508, __VMLINUX_SYMBOL_STR(drop_nlink) },
	{ 0xa675804c, __VMLINUX_SYMBOL_STR(utf8s_to_utf16s) },
	{ 0xa27ffa7d, __VMLINUX_SYMBOL_STR(mark_buffer_dirty_inode) },
	{ 0xf1c550e3, __VMLINUX_SYMBOL_STR(__mark_inode_dirty) },
	{ 0x83dd255b, __VMLINUX_SYMBOL_STR(dput) },
	{ 0xa4e434de, __VMLINUX_SYMBOL_STR(inc_nlink) },
	{ 0x6a026c70, __VMLINUX_SYMBOL_STR(d_find_alias) },
	{ 0x85a7fba7, __VMLINUX_SYMBOL_STR(names_cachep) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x190db1aa, __VMLINUX_SYMBOL_STR(mount_bdev) },
	{ 0x72c4874c, __VMLINUX_SYMBOL_STR(fat_sync_inode) },
	{ 0x85073d74, __VMLINUX_SYMBOL_STR(fat_add_entries) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x9cb76da5, __VMLINUX_SYMBOL_STR(fat_remove_entries) },
	{ 0xe297c0e7, __VMLINUX_SYMBOL_STR(fat_alloc_new_dir) },
	{ 0xe6a97b69, __VMLINUX_SYMBOL_STR(fat_fill_super) },
	{ 0x468b60b1, __VMLINUX_SYMBOL_STR(fat_build_inode) },
	{ 0x11089ac7, __VMLINUX_SYMBOL_STR(_ctype) },
	{ 0x820ce7b6, __VMLINUX_SYMBOL_STR(fat_attach) },
	{ 0x77fb7901, __VMLINUX_SYMBOL_STR(d_move) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x736c541f, __VMLINUX_SYMBOL_STR(set_nlink) },
	{ 0x941857c7, __VMLINUX_SYMBOL_STR(sync_dirty_buffer) },
	{ 0x282a2fb, __VMLINUX_SYMBOL_STR(fat_getattr) },
	{ 0xda87dc34, __VMLINUX_SYMBOL_STR(__brelse) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xb541366a, __VMLINUX_SYMBOL_STR(kill_block_super) },
	{ 0x6fceffbb, __VMLINUX_SYMBOL_STR(fat_search_long) },
	{ 0x6f20960a, __VMLINUX_SYMBOL_STR(full_name_hash) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x75448b8b, __VMLINUX_SYMBOL_STR(fat_scan) },
	{ 0x455b1dc2, __VMLINUX_SYMBOL_STR(register_filesystem) },
	{ 0xcedebf41, __VMLINUX_SYMBOL_STR(__fat_fs_error) },
	{ 0x78342ae0, __VMLINUX_SYMBOL_STR(iput) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xb63c5dd1, __VMLINUX_SYMBOL_STR(d_splice_alias) },
	{ 0xf5ec8ac2, __VMLINUX_SYMBOL_STR(fat_setattr) },
	{ 0xd868f4, __VMLINUX_SYMBOL_STR(fat_free_clusters) },
	{ 0xaae6f4d8, __VMLINUX_SYMBOL_STR(fat_get_dotdot_entry) },
	{ 0xa009b3e7, __VMLINUX_SYMBOL_STR(unregister_filesystem) },
	{ 0x96191bca, __VMLINUX_SYMBOL_STR(fat_time_unix2fat) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x3debaff8, __VMLINUX_SYMBOL_STR(fat_dir_empty) },
	{ 0x1bce8f24, __VMLINUX_SYMBOL_STR(d_instantiate) },
	{ 0x9ac23dd2, __VMLINUX_SYMBOL_STR(clear_nlink) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=fat";


MODULE_INFO(srcversion, "7A6419AD0D3CC89661A9B60");
