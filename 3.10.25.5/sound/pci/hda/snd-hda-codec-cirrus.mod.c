#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x3445a13, __VMLINUX_SYMBOL_STR(snd_hda_gen_free) },
	{ 0x1bf65c9c, __VMLINUX_SYMBOL_STR(snd_hda_gen_parse_auto_config) },
	{ 0x900f7006, __VMLINUX_SYMBOL_STR(snd_hda_apply_fixup) },
	{ 0x4555e99c, __VMLINUX_SYMBOL_STR(snd_hda_pick_fixup) },
	{ 0xb7d17e61, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_pcms) },
	{ 0xce155efb, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect) },
	{ 0x588dd561, __VMLINUX_SYMBOL_STR(snd_hda_jack_unsol_event) },
	{ 0x1e88a7c2, __VMLINUX_SYMBOL_STR(snd_hda_codec_set_pincfg) },
	{ 0x289ee3e4, __VMLINUX_SYMBOL_STR(snd_hda_gen_update_outputs) },
	{ 0xf872af8d, __VMLINUX_SYMBOL_STR(snd_hda_jack_detect_enable_callback) },
	{ 0x1253059c, __VMLINUX_SYMBOL_STR(_snd_hda_set_pin_ctl) },
	{ 0x2385cdde, __VMLINUX_SYMBOL_STR(snd_hda_add_codec_preset) },
	{ 0x8c064de3, __VMLINUX_SYMBOL_STR(snd_hda_codec_write) },
	{ 0x5698ae47, __VMLINUX_SYMBOL_STR(snd_hda_gen_init) },
	{ 0x8fe7eb13, __VMLINUX_SYMBOL_STR(snd_hda_parse_pin_defcfg) },
	{ 0x10311d9c, __VMLINUX_SYMBOL_STR(snd_hda_override_amp_caps) },
	{ 0xacd73f87, __VMLINUX_SYMBOL_STR(snd_hda_sequence_write) },
	{ 0x4d59dc0d, __VMLINUX_SYMBOL_STR(snd_hda_ctl_add) },
	{ 0xfa4ea467, __VMLINUX_SYMBOL_STR(snd_ctl_new1) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x9c27f124, __VMLINUX_SYMBOL_STR(snd_hda_codec_get_pincfg) },
	{ 0x551252dd, __VMLINUX_SYMBOL_STR(snd_hda_gen_spec_init) },
	{ 0xebafbd63, __VMLINUX_SYMBOL_STR(snd_hda_delete_codec_preset) },
	{ 0xea5594e3, __VMLINUX_SYMBOL_STR(query_amp_caps) },
	{ 0x29da3d99, __VMLINUX_SYMBOL_STR(snd_hda_gen_build_controls) },
	{ 0x31da3d1f, __VMLINUX_SYMBOL_STR(snd_hda_codec_read) },
	{ 0x2479273c, __VMLINUX_SYMBOL_STR(snd_hda_shutup_pins) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-hda-codec,snd";


MODULE_INFO(srcversion, "FAE3198F3D9A75AFEC3553B");
