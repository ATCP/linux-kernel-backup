#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x7b7045cd, __VMLINUX_SYMBOL_STR(snd_sbdsp_create) },
	{ 0x1fedf0f4, __VMLINUX_SYMBOL_STR(__request_region) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xbcf5587b, __VMLINUX_SYMBOL_STR(snd_pcm_period_elapsed) },
	{     0xfb, __VMLINUX_SYMBOL_STR(snd_sbdsp_reset) },
	{ 0xb4df1b5c, __VMLINUX_SYMBOL_STR(snd_card_create) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0xb72687ed, __VMLINUX_SYMBOL_STR(snd_opl3_hwdep_new) },
	{ 0x59df21ad, __VMLINUX_SYMBOL_STR(pci_disable_device) },
	{ 0xd1784022, __VMLINUX_SYMBOL_STR(snd_mpu401_uart_new) },
	{ 0x33339182, __VMLINUX_SYMBOL_STR(pci_release_regions) },
	{ 0x1976aa06, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xd97b7cd3, __VMLINUX_SYMBOL_STR(snd_card_disconnect) },
	{ 0x36a744c1, __VMLINUX_SYMBOL_STR(snd_sbmixer_suspend) },
	{ 0xcb58edff, __VMLINUX_SYMBOL_STR(snd_pcm_suspend_all) },
	{ 0x6640df15, __VMLINUX_SYMBOL_STR(snd_sbmixer_read) },
	{ 0x1d027e4b, __VMLINUX_SYMBOL_STR(snd_pcm_format_signed) },
	{ 0x35b6b772, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0xc0fec227, __VMLINUX_SYMBOL_STR(snd_mpu401_uart_interrupt) },
	{ 0xff7559e4, __VMLINUX_SYMBOL_STR(ioport_resource) },
	{ 0xc2ac79c5, __VMLINUX_SYMBOL_STR(pci_restore_state) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x1a8e40c, __VMLINUX_SYMBOL_STR(snd_pcm_set_ops) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x4bd795e7, __VMLINUX_SYMBOL_STR(snd_pcm_lib_free_pages) },
	{ 0x2ae3deaa, __VMLINUX_SYMBOL_STR(release_and_free_resource) },
	{ 0xe086d97e, __VMLINUX_SYMBOL_STR(snd_pcm_lib_ioctl) },
	{ 0xe1328d8c, __VMLINUX_SYMBOL_STR(__gameport_register_port) },
	{ 0x3f53f985, __VMLINUX_SYMBOL_STR(snd_pcm_lib_malloc_pages) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x15d287a8, __VMLINUX_SYMBOL_STR(gameport_unregister_port) },
	{ 0x80c6cdc6, __VMLINUX_SYMBOL_STR(snd_sbmixer_new) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xd52bf1ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xec559155, __VMLINUX_SYMBOL_STR(snd_opl3_create) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xb555a29a, __VMLINUX_SYMBOL_STR(pci_set_power_state) },
	{ 0xbd49befc, __VMLINUX_SYMBOL_STR(snd_sbdsp_command) },
	{ 0x68a24153, __VMLINUX_SYMBOL_STR(snd_pcm_format_physical_width) },
	{ 0x12bd6c9, __VMLINUX_SYMBOL_STR(pci_request_regions) },
	{ 0x4845c423, __VMLINUX_SYMBOL_STR(param_array_ops) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x64c65f8d, __VMLINUX_SYMBOL_STR(snd_pcm_lib_preallocate_pages_for_all) },
	{ 0xf37f2e3, __VMLINUX_SYMBOL_STR(snd_card_free) },
	{ 0xf774757e, __VMLINUX_SYMBOL_STR(snd_card_register) },
	{ 0xa8228593, __VMLINUX_SYMBOL_STR(snd_pcm_new) },
	{ 0x9647a668, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xfa21ad68, __VMLINUX_SYMBOL_STR(snd_sbmixer_resume) },
	{ 0xa83400fd, __VMLINUX_SYMBOL_STR(gameport_set_phys) },
	{ 0x9824d2eb, __VMLINUX_SYMBOL_STR(snd_sbmixer_write) },
	{ 0xea4b8c8a, __VMLINUX_SYMBOL_STR(pci_save_state) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd-sb-common,snd-pcm,snd,snd-opl3-lib,snd-mpu401-uart,gameport";

MODULE_ALIAS("pci:v00004005d00004000sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "2F9FE3836643BD873C680AD");
