#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb218d949, __VMLINUX_SYMBOL_STR(blk_queue_merge_bvec) },
	{ 0xfb994d5a, __VMLINUX_SYMBOL_STR(alloc_pages_current) },
	{ 0x9b0a1716, __VMLINUX_SYMBOL_STR(bio_split) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x13531bc6, __VMLINUX_SYMBOL_STR(submit_bio_wait) },
	{ 0xb9eb8335, __VMLINUX_SYMBOL_STR(blk_queue_io_opt) },
	{ 0x8dcf0d6f, __VMLINUX_SYMBOL_STR(bio_alloc_bioset) },
	{ 0x5a27d7be, __VMLINUX_SYMBOL_STR(mddev_congested) },
	{ 0xd0727754, __VMLINUX_SYMBOL_STR(rdev_set_badblocks) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x412fc7c2, __VMLINUX_SYMBOL_STR(md_is_badblock) },
	{ 0xb2ae2921, __VMLINUX_SYMBOL_STR(bio_clone_mddev) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x1a18041, __VMLINUX_SYMBOL_STR(bio_alloc_mddev) },
	{ 0xbcd76cee, __VMLINUX_SYMBOL_STR(md_check_recovery) },
	{ 0x6e0fef1c, __VMLINUX_SYMBOL_STR(blk_queue_io_min) },
	{ 0x2fc4703e, __VMLINUX_SYMBOL_STR(md_write_end) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x7a8ae8b8, __VMLINUX_SYMBOL_STR(bitmap_endwrite) },
	{ 0xefba93e1, __VMLINUX_SYMBOL_STR(mempool_destroy) },
	{ 0xabe2b7c5, __VMLINUX_SYMBOL_STR(bitmap_unplug) },
	{ 0x8a7af99e, __VMLINUX_SYMBOL_STR(md_new_event) },
	{ 0x83232a8c, __VMLINUX_SYMBOL_STR(unregister_md_personality) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x121f115c, __VMLINUX_SYMBOL_STR(revalidate_disk) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x3ad29196, __VMLINUX_SYMBOL_STR(bitmap_resize) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x63900dcf, __VMLINUX_SYMBOL_STR(bio_reset) },
	{ 0xdbebb65b, __VMLINUX_SYMBOL_STR(bitmap_start_sync) },
	{ 0xd27b25dd, __VMLINUX_SYMBOL_STR(blk_check_plugged) },
	{ 0xb85967f0, __VMLINUX_SYMBOL_STR(md_register_thread) },
	{ 0xd3fe4960, __VMLINUX_SYMBOL_STR(md_flush_request) },
	{ 0x8e9a095f, __VMLINUX_SYMBOL_STR(bio_pair_release) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xd1de10e4, __VMLINUX_SYMBOL_STR(blk_queue_max_discard_sectors) },
	{ 0x5ee032ea, __VMLINUX_SYMBOL_STR(bio_add_page) },
	{ 0x117c43f3, __VMLINUX_SYMBOL_STR(md_do_sync) },
	{ 0xa7f46bcd, __VMLINUX_SYMBOL_STR(disk_stack_limits) },
	{ 0x92c51dc2, __VMLINUX_SYMBOL_STR(bitmap_close_sync) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xec33e60e, __VMLINUX_SYMBOL_STR(md_trim_bio) },
	{ 0xdadcff4e, __VMLINUX_SYMBOL_STR(blk_sync_queue) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xc2cdbf1, __VMLINUX_SYMBOL_STR(synchronize_sched) },
	{ 0xc06ff90, __VMLINUX_SYMBOL_STR(generic_make_request) },
	{ 0x71a84b0e, __VMLINUX_SYMBOL_STR(md_wait_for_blocked_rdev) },
	{ 0x8d48a0fe, __VMLINUX_SYMBOL_STR(__get_page_tail) },
	{ 0x83ea34b3, __VMLINUX_SYMBOL_STR(md_integrity_add_rdev) },
	{ 0xe0794ec4, __VMLINUX_SYMBOL_STR(bio_endio) },
	{ 0x403fbba, __VMLINUX_SYMBOL_STR(bio_put) },
	{ 0xf3cb9705, __VMLINUX_SYMBOL_STR(md_done_sync) },
	{ 0x9475bb71, __VMLINUX_SYMBOL_STR(sysfs_create_link) },
	{ 0x13b715e2, __VMLINUX_SYMBOL_STR(blk_finish_plug) },
	{ 0xc5daa10b, __VMLINUX_SYMBOL_STR(bitmap_cond_end_sync) },
	{ 0xe9dff136, __VMLINUX_SYMBOL_STR(mempool_alloc) },
	{ 0x9b5c1e5f, __VMLINUX_SYMBOL_STR(md_write_start) },
	{ 0x94c90b31, __VMLINUX_SYMBOL_STR(bdevname) },
	{ 0xf3d3b8c3, __VMLINUX_SYMBOL_STR(sysfs_notify_dirent) },
	{ 0xbe6c05b6, __VMLINUX_SYMBOL_STR(rdev_clear_badblocks) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x79a38e61, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0x84814ae6, __VMLINUX_SYMBOL_STR(md_set_array_sectors) },
	{ 0x3f71faa1, __VMLINUX_SYMBOL_STR(mempool_create) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x9ad81936, __VMLINUX_SYMBOL_STR(sysfs_notify) },
	{ 0x75ad5aec, __VMLINUX_SYMBOL_STR(md_wakeup_thread) },
	{ 0xcc5005fe, __VMLINUX_SYMBOL_STR(msleep_interruptible) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x4aad52d7, __VMLINUX_SYMBOL_STR(mempool_free) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xce46e140, __VMLINUX_SYMBOL_STR(ktime_get_ts) },
	{ 0xcf21d241, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xb3f7646e, __VMLINUX_SYMBOL_STR(kthread_should_stop) },
	{ 0x91babda7, __VMLINUX_SYMBOL_STR(sync_page_io) },
	{ 0x2148c45b, __VMLINUX_SYMBOL_STR(md_unregister_thread) },
	{ 0x3460e42e, __VMLINUX_SYMBOL_STR(md_finish_reshape) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xde8eb015, __VMLINUX_SYMBOL_STR(bitmap_startwrite) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0x1b82fefa, __VMLINUX_SYMBOL_STR(md_unplug) },
	{ 0xd8eeeec8, __VMLINUX_SYMBOL_STR(md_error) },
	{ 0x2e710893, __VMLINUX_SYMBOL_STR(put_page) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x65d67a20, __VMLINUX_SYMBOL_STR(blk_start_plug) },
	{ 0x57012a, __VMLINUX_SYMBOL_STR(blk_queue_max_write_same_sectors) },
	{ 0xbb009569, __VMLINUX_SYMBOL_STR(md_integrity_register) },
	{ 0xd72d6641, __VMLINUX_SYMBOL_STR(register_md_personality) },
	{ 0xf334dc5a, __VMLINUX_SYMBOL_STR(bitmap_end_sync) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "4B85EE4B3356715740C8F22");
