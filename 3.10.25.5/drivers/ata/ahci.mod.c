#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x4054e1b7, __VMLINUX_SYMBOL_STR(ahci_sdev_attrs) },
	{ 0xfa7a5a36, __VMLINUX_SYMBOL_STR(ahci_shost_attrs) },
	{ 0x63205de1, __VMLINUX_SYMBOL_STR(ata_scsi_unlock_native_capacity) },
	{ 0x32d436c7, __VMLINUX_SYMBOL_STR(ata_std_bios_param) },
	{ 0xd32fe193, __VMLINUX_SYMBOL_STR(ata_scsi_change_queue_depth) },
	{ 0x94a68723, __VMLINUX_SYMBOL_STR(ata_scsi_slave_destroy) },
	{ 0x69e9a2cb, __VMLINUX_SYMBOL_STR(ata_scsi_slave_config) },
	{ 0xcc94722f, __VMLINUX_SYMBOL_STR(ata_scsi_queuecmd) },
	{ 0x39a0cbce, __VMLINUX_SYMBOL_STR(ata_scsi_ioctl) },
	{ 0xa177e094, __VMLINUX_SYMBOL_STR(ahci_pmp_retry_srst_ops) },
	{ 0x16b9dce1, __VMLINUX_SYMBOL_STR(ahci_ops) },
	{ 0x5f1d5630, __VMLINUX_SYMBOL_STR(ata_pci_remove_one) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x33374b1f, __VMLINUX_SYMBOL_STR(ata_host_activate) },
	{ 0x5eacba1f, __VMLINUX_SYMBOL_STR(ahci_interrupt) },
	{ 0xe21a9d7f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x9e3b862f, __VMLINUX_SYMBOL_STR(ahci_print_info) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0xe9a54ed0, __VMLINUX_SYMBOL_STR(ata_link_next) },
	{ 0x3e782391, __VMLINUX_SYMBOL_STR(ata_dev_next) },
	{ 0xd4835ef8, __VMLINUX_SYMBOL_STR(dmi_check_system) },
	{ 0xba3a7316, __VMLINUX_SYMBOL_STR(ata_dummy_port_ops) },
	{ 0x77f230ad, __VMLINUX_SYMBOL_STR(ata_port_pbar_desc) },
	{ 0x10f33892, __VMLINUX_SYMBOL_STR(ahci_reset_em) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xea50dad3, __VMLINUX_SYMBOL_STR(ahci_ignore_sss) },
	{ 0xe982fc75, __VMLINUX_SYMBOL_STR(ata_host_alloc_pinfo) },
	{ 0x940aa9, __VMLINUX_SYMBOL_STR(ahci_set_em_messages) },
	{ 0xa440972, __VMLINUX_SYMBOL_STR(ahci_save_initial_config) },
	{ 0xffde746c, __VMLINUX_SYMBOL_STR(pcim_iomap_table) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x37db8f19, __VMLINUX_SYMBOL_STR(dmi_get_date) },
	{ 0x50877b9, __VMLINUX_SYMBOL_STR(dmi_first_match) },
	{ 0xa9bd7b88, __VMLINUX_SYMBOL_STR(devm_kzalloc) },
	{ 0xe722ff65, __VMLINUX_SYMBOL_STR(pci_bus_read_config_byte) },
	{ 0xa6cec0d, __VMLINUX_SYMBOL_STR(pcim_pin_device) },
	{ 0xaecb07ec, __VMLINUX_SYMBOL_STR(pcim_iomap_regions_request_all) },
	{ 0xeb8d93d2, __VMLINUX_SYMBOL_STR(pcim_enable_device) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x7f2a05ca, __VMLINUX_SYMBOL_STR(ata_print_version) },
	{ 0xc09a6f21, __VMLINUX_SYMBOL_STR(pci_intx) },
	{ 0x98616114, __VMLINUX_SYMBOL_STR(pci_enable_msi_block) },
	{ 0x35e582ea, __VMLINUX_SYMBOL_STR(pci_disable_msi) },
	{ 0xcf267ce7, __VMLINUX_SYMBOL_STR(pci_enable_msi_block_auto) },
	{ 0x42fe8d1f, __VMLINUX_SYMBOL_STR(devm_free_irq) },
	{ 0x8665b2a, __VMLINUX_SYMBOL_STR(ata_host_register) },
	{ 0x48a2b366, __VMLINUX_SYMBOL_STR(ata_port_desc) },
	{ 0xe3a747a4, __VMLINUX_SYMBOL_STR(devm_request_threaded_irq) },
	{ 0x6e05c4e7, __VMLINUX_SYMBOL_STR(ahci_hw_interrupt) },
	{ 0x694f923e, __VMLINUX_SYMBOL_STR(ahci_thread_fn) },
	{ 0x9480348c, __VMLINUX_SYMBOL_STR(dev_driver_string) },
	{ 0x48fc7296, __VMLINUX_SYMBOL_STR(ata_host_start) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x32dc70be, __VMLINUX_SYMBOL_STR(ahci_kick_engine) },
	{ 0x70d5278a, __VMLINUX_SYMBOL_STR(ata_wait_after_reset) },
	{ 0xea6f5409, __VMLINUX_SYMBOL_STR(ahci_check_ready) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x8b752ac1, __VMLINUX_SYMBOL_STR(ata_tf_to_fis) },
	{ 0xcb202bee, __VMLINUX_SYMBOL_STR(ahci_start_engine) },
	{ 0xebaeb744, __VMLINUX_SYMBOL_STR(sata_link_hardreset) },
	{ 0x4fdc945d, __VMLINUX_SYMBOL_STR(sata_deb_timing_normal) },
	{ 0xcccfb2fa, __VMLINUX_SYMBOL_STR(sata_deb_timing_hotplug) },
	{ 0xa7606d35, __VMLINUX_SYMBOL_STR(ahci_stop_engine) },
	{ 0x4393d5d4, __VMLINUX_SYMBOL_STR(ata_pci_device_suspend) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x96affe9b, __VMLINUX_SYMBOL_STR(ata_host_resume) },
	{ 0xb2d449ca, __VMLINUX_SYMBOL_STR(ata_pci_device_do_resume) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x2aa84582, __VMLINUX_SYMBOL_STR(ahci_init_controller) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0xc67912fe, __VMLINUX_SYMBOL_STR(ahci_reset_controller) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libahci,libata";

MODULE_ALIAS("pci:v00008086d00002652sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002653sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000027C1sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000027C5sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000027C3sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010B9d00005288sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002681sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002682sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002683sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000027C6sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002821sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002822sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002824sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002829sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000282Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002922sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002923sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002924sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002925sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002927sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002929sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000292Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000292Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000292Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000292Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000294Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000294Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000502Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000502Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003A05sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003A22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003A25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B23sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B24sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B29sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B2Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B2Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003B2Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001C02sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001C03sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001C04sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001C05sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001C06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001C07sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001D02sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001D04sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001D06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002826sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002323sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E02sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E03sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E04sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E05sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E07sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001E0Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C02sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C03sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C04sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C05sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C07sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C0Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008C0Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C02sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C03sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C04sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C05sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C07sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C0Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C0Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F23sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F24sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F26sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F27sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F2Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F2Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F32sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F33sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F34sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F35sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F36sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F37sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F3Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001F3Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002823sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002827sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D02sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D04sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D0Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D62sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D64sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D66sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00008D6Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000023A3sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C83sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C85sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C87sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00009C8Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v0000197Bd*sv*sd*bc01sc06i01*");
MODULE_ALIAS("pci:v0000197Bd00002362sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v0000197Bd0000236Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004380sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004390sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004391sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004392sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004393sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004394sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001002d00004395sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d00007800sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d00007900sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001022d*sv*sd*bc01sc04i00*");
MODULE_ALIAS("pci:v00001106d00003349sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001106d00006287sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000044Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000044Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000044Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000044Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000045Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000045Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000045Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000045Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000550sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000551sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000552sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000553sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000554sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000555sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000556sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000557sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000558sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000559sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000055Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000055Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000580sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000581sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000582sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000583sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000584sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000585sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000586sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000587sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000588sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000589sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000058Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000058Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000058Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000058Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000058Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd0000058Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F0sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F1sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F2sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F3sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F4sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F5sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F6sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F7sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F8sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007F9sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007FAsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd000007FBsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD0sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD1sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD2sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD3sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD4sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD5sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD6sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD7sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD8sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AD9sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ADAsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ADBsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AB4sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AB5sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AB6sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AB7sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AB8sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000AB9sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ABAsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ABBsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ABCsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ABDsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ABEsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000ABFsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D84sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D85sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D86sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D87sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D88sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D89sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D8Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D8Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D8Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D8Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D8Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000010DEd00000D8Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001039d00001184sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001039d00001185sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001039d00000186sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v0000104Ad0000CC06sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006145sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000011ABd00006121sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B4Bd00009123sv*sd*bc01sc06i01*");
MODULE_ALIAS("pci:v00001B4Bd00009125sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B4Bd0000917Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B4Bd00009172sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B4Bd00009192sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B4Bd000091A3sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B4Bd00009230sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v0000105Ad00003F20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B21d00000601sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B21d00000602sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B21d00000611sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B21d00000612sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001C44d00008000sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v*d*sv*sd*bc01sc06i01*");

MODULE_INFO(srcversion, "17831B1AA8191AB5EBA0C86");
