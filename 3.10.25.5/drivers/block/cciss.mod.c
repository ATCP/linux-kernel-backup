#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x429dc6b5, __VMLINUX_SYMBOL_STR(seq_release) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x7f76e4a5, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0x435e0b50, __VMLINUX_SYMBOL_STR(proc_mkdir) },
	{ 0x71a50dbc, __VMLINUX_SYMBOL_STR(register_blkdev) },
	{ 0x4a8cbbae, __VMLINUX_SYMBOL_STR(dma_set_mask) },
	{ 0xf432dd3d, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x46e7e82d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xf65f7fc7, __VMLINUX_SYMBOL_STR(dma_supported) },
	{ 0xb3f7646e, __VMLINUX_SYMBOL_STR(kthread_should_stop) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xcaa7eb52, __VMLINUX_SYMBOL_STR(copy_in_user) },
	{ 0x6729d3df, __VMLINUX_SYMBOL_STR(__get_user_4) },
	{ 0x8f9c199c, __VMLINUX_SYMBOL_STR(__get_user_2) },
	{ 0xbffde8ec, __VMLINUX_SYMBOL_STR(compat_alloc_user_space) },
	{ 0xc4148678, __VMLINUX_SYMBOL_STR(scsi_cmd_blk_ioctl) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x4302d0eb, __VMLINUX_SYMBOL_STR(free_pages) },
	{ 0x93fca811, __VMLINUX_SYMBOL_STR(__get_free_pages) },
	{ 0x67365a5c, __VMLINUX_SYMBOL_STR(scsi_scan_host) },
	{ 0x51973243, __VMLINUX_SYMBOL_STR(scsi_add_host_with_dma) },
	{ 0xaddb8893, __VMLINUX_SYMBOL_STR(scsi_host_alloc) },
	{ 0xecd30647, __VMLINUX_SYMBOL_STR(scsi_add_device) },
	{ 0x83338307, __VMLINUX_SYMBOL_STR(scsi_device_put) },
	{ 0x230885ed, __VMLINUX_SYMBOL_STR(scsi_remove_device) },
	{ 0x6c95f70b, __VMLINUX_SYMBOL_STR(scsi_device_lookup) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0xea4b8c8a, __VMLINUX_SYMBOL_STR(pci_save_state) },
	{ 0x59df21ad, __VMLINUX_SYMBOL_STR(pci_disable_device) },
	{ 0xc2ac79c5, __VMLINUX_SYMBOL_STR(pci_restore_state) },
	{ 0x24939c32, __VMLINUX_SYMBOL_STR(pci_bus_write_config_word) },
	{ 0x952c5e83, __VMLINUX_SYMBOL_STR(pci_bus_read_config_word) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x21ca769b, __VMLINUX_SYMBOL_STR(pci_bus_write_config_dword) },
	{ 0x28557954, __VMLINUX_SYMBOL_STR(pci_bus_read_config_dword) },
	{ 0x2a303d4d, __VMLINUX_SYMBOL_STR(check_signature) },
	{ 0x98616114, __VMLINUX_SYMBOL_STR(pci_enable_msi_block) },
	{ 0x71500d3d, __VMLINUX_SYMBOL_STR(pci_enable_msix) },
	{ 0xa765d721, __VMLINUX_SYMBOL_STR(pci_find_capability) },
	{ 0x12bd6c9, __VMLINUX_SYMBOL_STR(pci_request_regions) },
	{ 0x9647a668, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0xb8d0055e, __VMLINUX_SYMBOL_STR(pci_disable_link_state) },
	{ 0xfab51f4f, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x9206f91c, __VMLINUX_SYMBOL_STR(kthread_create_on_node) },
	{ 0xf106d9d3, __VMLINUX_SYMBOL_STR(bus_register) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xdc8505bb, __VMLINUX_SYMBOL_STR(bus_unregister) },
	{ 0x15b52968, __VMLINUX_SYMBOL_STR(kthread_stop) },
	{ 0xefa7be8f, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x33339182, __VMLINUX_SYMBOL_STR(pci_release_regions) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0x35e582ea, __VMLINUX_SYMBOL_STR(pci_disable_msi) },
	{ 0x67402f87, __VMLINUX_SYMBOL_STR(pci_disable_msix) },
	{ 0x28fd2cd3, __VMLINUX_SYMBOL_STR(scsi_host_put) },
	{ 0x5af82fa6, __VMLINUX_SYMBOL_STR(scsi_remove_host) },
	{ 0xb5a459dc, __VMLINUX_SYMBOL_STR(unregister_blkdev) },
	{ 0x90ce81d5, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0x40256835, __VMLINUX_SYMBOL_STR(complete_all) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x4b06d2e7, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x96469662, __VMLINUX_SYMBOL_STR(blk_complete_request) },
	{ 0x1031b7e9, __VMLINUX_SYMBOL_STR(scsi_dma_unmap) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x9ec1ec3b, __VMLINUX_SYMBOL_STR(scsi_dma_map) },
	{ 0x6c137fc1, __VMLINUX_SYMBOL_STR(scsi_cmd_get_serial) },
	{ 0xd0ee38b8, __VMLINUX_SYMBOL_STR(schedule_timeout_uninterruptible) },
	{ 0x72ea7b2d, __VMLINUX_SYMBOL_STR(scsi_device_type) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0x17a54f1c, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0x620ec38d, __VMLINUX_SYMBOL_STR(device_del) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0xa2cd8a7a, __VMLINUX_SYMBOL_STR(blk_stop_queue) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x51e8a6e0, __VMLINUX_SYMBOL_STR(blk_rq_map_sg) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0xe94b430c, __VMLINUX_SYMBOL_STR(blk_start_request) },
	{ 0xfa801eb, __VMLINUX_SYMBOL_STR(blk_peek_request) },
	{ 0xbf899fdb, __VMLINUX_SYMBOL_STR(blk_start_queue) },
	{ 0x3ddcdebf, __VMLINUX_SYMBOL_STR(blk_end_request_all) },
	{ 0x71de9b3f, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x77e2f33, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0x2e133e3e, __VMLINUX_SYMBOL_STR(blk_cleanup_queue) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0x5ce6d1ea, __VMLINUX_SYMBOL_STR(blk_queue_logical_block_size) },
	{ 0x1c68c7ba, __VMLINUX_SYMBOL_STR(blk_queue_softirq_done) },
	{ 0xd8f9f68c, __VMLINUX_SYMBOL_STR(blk_queue_max_hw_sectors) },
	{ 0x6a6feb36, __VMLINUX_SYMBOL_STR(blk_queue_max_segments) },
	{ 0x664d950c, __VMLINUX_SYMBOL_STR(blk_queue_bounce_limit) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x62d06521, __VMLINUX_SYMBOL_STR(blk_init_queue) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x7932f22c, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0x9839dada, __VMLINUX_SYMBOL_STR(seq_open) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xf11543ff, __VMLINUX_SYMBOL_STR(find_first_zero_bit) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0x15ddbdc, __VMLINUX_SYMBOL_STR(wait_for_completion_interruptible) },
	{ 0x81b02193, __VMLINUX_SYMBOL_STR(wake_up_process) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x50a58e8e, __VMLINUX_SYMBOL_STR(mutex_trylock) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x20d3df20, __VMLINUX_SYMBOL_STR(device_add) },
	{ 0x291f89b7, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0xb28c531c, __VMLINUX_SYMBOL_STR(device_initialize) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xc2e587d1, __VMLINUX_SYMBOL_STR(reset_devices) },
	{ 0xf799caad, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0xe2f71ad8, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x6d0aba34, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x65e75cb6, __VMLINUX_SYMBOL_STR(__list_del_entry) },
	{ 0xb84c2ce2, __VMLINUX_SYMBOL_STR(x86_dma_fallback_dev) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x4c9d28b0, __VMLINUX_SYMBOL_STR(phys_base) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x82cf3c61, __VMLINUX_SYMBOL_STR(dev_warn) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=scsi_mod";

MODULE_ALIAS("pci:v00000E11d0000B060sv00000E11sd00004070bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000B178sv00000E11sd00004080bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000B178sv00000E11sd00004082bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d0000B178sv00000E11sd00004083bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d00000046sv00000E11sd00004091bc*sc*i*");
MODULE_ALIAS("pci:v00000E11d00000046sv00000E11sd0000409Abc*sc*i*");
MODULE_ALIAS("pci:v00000E11d00000046sv00000E11sd0000409Bbc*sc*i*");
MODULE_ALIAS("pci:v00000E11d00000046sv00000E11sd0000409Cbc*sc*i*");
MODULE_ALIAS("pci:v00000E11d00000046sv00000E11sd0000409Dbc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003220sv0000103Csd00003225bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003230sv0000103Csd00003223bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003230sv0000103Csd00003234bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003230sv0000103Csd00003235bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003238sv0000103Csd00003211bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003238sv0000103Csd00003212bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003238sv0000103Csd00003213bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003238sv0000103Csd00003214bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003238sv0000103Csd00003215bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003230sv0000103Csd00003237bc*sc*i*");
MODULE_ALIAS("pci:v0000103Cd00003230sv0000103Csd0000323Dbc*sc*i*");

MODULE_INFO(srcversion, "7FD4BAF190083867FFB8ECE");
