#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x6942caeb, __VMLINUX_SYMBOL_STR(iscsi_change_queue_depth) },
	{ 0xb11f2182, __VMLINUX_SYMBOL_STR(iscsi_target_alloc) },
	{ 0xafdce62b, __VMLINUX_SYMBOL_STR(iscsi_eh_recover_target) },
	{ 0xb5107020, __VMLINUX_SYMBOL_STR(iscsi_eh_device_reset) },
	{ 0x5e85ce11, __VMLINUX_SYMBOL_STR(iscsi_eh_abort) },
	{ 0xb08e5d8c, __VMLINUX_SYMBOL_STR(iscsi_queuecommand) },
	{ 0xef346615, __VMLINUX_SYMBOL_STR(cxgbi_attr_is_visible) },
	{ 0x111fcc91, __VMLINUX_SYMBOL_STR(cxgbi_ep_disconnect) },
	{ 0x19107bc3, __VMLINUX_SYMBOL_STR(cxgbi_ep_poll) },
	{ 0xb2adcbbb, __VMLINUX_SYMBOL_STR(cxgbi_ep_connect) },
	{ 0x65432038, __VMLINUX_SYMBOL_STR(iscsi_session_recovery_timedout) },
	{ 0x52cfa829, __VMLINUX_SYMBOL_STR(cxgbi_parse_pdu_itt) },
	{ 0xe9861fe8, __VMLINUX_SYMBOL_STR(cxgbi_conn_init_pdu) },
	{ 0xfd7216d8, __VMLINUX_SYMBOL_STR(cxgbi_conn_xmit_pdu) },
	{ 0xe9eaeab6, __VMLINUX_SYMBOL_STR(cxgbi_conn_alloc_pdu) },
	{ 0x2e388b5e, __VMLINUX_SYMBOL_STR(cxgbi_cleanup_task) },
	{ 0x9fded913, __VMLINUX_SYMBOL_STR(iscsi_tcp_task_xmit) },
	{ 0x79b42460, __VMLINUX_SYMBOL_STR(iscsi_tcp_task_init) },
	{ 0x577a1f84, __VMLINUX_SYMBOL_STR(cxgbi_get_conn_stats) },
	{ 0x517ccd47, __VMLINUX_SYMBOL_STR(iscsi_conn_send_pdu) },
	{ 0x2edf0b9c, __VMLINUX_SYMBOL_STR(cxgbi_set_host_param) },
	{ 0xdffabeb5, __VMLINUX_SYMBOL_STR(cxgbi_get_host_param) },
	{ 0xf4b4b747, __VMLINUX_SYMBOL_STR(iscsi_session_get_param) },
	{ 0x74fac18a, __VMLINUX_SYMBOL_STR(iscsi_conn_get_param) },
	{ 0xff88bc44, __VMLINUX_SYMBOL_STR(cxgbi_get_ep_param) },
	{ 0x17cc787f, __VMLINUX_SYMBOL_STR(cxgbi_set_conn_param) },
	{ 0xfd1ada6e, __VMLINUX_SYMBOL_STR(iscsi_tcp_conn_teardown) },
	{ 0x173eb2d3, __VMLINUX_SYMBOL_STR(iscsi_conn_stop) },
	{ 0x1440fa2f, __VMLINUX_SYMBOL_STR(iscsi_conn_start) },
	{ 0x770e4cf4, __VMLINUX_SYMBOL_STR(cxgbi_bind_conn) },
	{ 0x2cbe7b23, __VMLINUX_SYMBOL_STR(cxgbi_create_conn) },
	{ 0x9ad9a11f, __VMLINUX_SYMBOL_STR(cxgbi_destroy_session) },
	{ 0x7aa47089, __VMLINUX_SYMBOL_STR(cxgbi_create_session) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x40e46dbf, __VMLINUX_SYMBOL_STR(cxgb3_remove_tid) },
	{ 0x687913e7, __VMLINUX_SYMBOL_STR(t3_l2e_free) },
	{ 0x3773116a, __VMLINUX_SYMBOL_STR(cxgbi_sock_purge_wr_queue) },
	{ 0xb3fa91ba, __VMLINUX_SYMBOL_STR(cxgbi_sock_skb_entail) },
	{ 0xa68c9595, __VMLINUX_SYMBOL_STR(cxgbi_sock_free_cpl_skbs) },
	{ 0xe93474f0, __VMLINUX_SYMBOL_STR(cxgbi_sock_select_mss) },
	{ 0x60cfbfb2, __VMLINUX_SYMBOL_STR(cxgb3_alloc_atid) },
	{ 0x9084d93a, __VMLINUX_SYMBOL_STR(t3_l2t_get) },
	{ 0xca1d920d, __VMLINUX_SYMBOL_STR(cxgbi_hbas_add) },
	{ 0x3a5c4389, __VMLINUX_SYMBOL_STR(cxgbi_ddp_page_size_factor) },
	{ 0xf772371c, __VMLINUX_SYMBOL_STR(cxgbi_ddp_init) },
	{ 0xd3bb332a, __VMLINUX_SYMBOL_STR(cxgbi_device_register) },
	{ 0xe4128246, __VMLINUX_SYMBOL_STR(cxgbi_device_portmap_create) },
	{ 0x6a95b683, __VMLINUX_SYMBOL_STR(cxgb3_register_client) },
	{ 0xeae833d4, __VMLINUX_SYMBOL_STR(cxgbi_iscsi_init) },
	{ 0xae170792, __VMLINUX_SYMBOL_STR(cxgbi_iscsi_cleanup) },
	{ 0x7fdb6004, __VMLINUX_SYMBOL_STR(cxgbi_device_unregister_all) },
	{ 0xa40f197d, __VMLINUX_SYMBOL_STR(cxgb3_unregister_client) },
	{ 0xf6efa5d0, __VMLINUX_SYMBOL_STR(cxgbi_sock_rcv_peer_close) },
	{ 0x2dd8590f, __VMLINUX_SYMBOL_STR(cxgbi_sock_closed) },
	{ 0x988d4474, __VMLINUX_SYMBOL_STR(cxgbi_sock_rcv_abort_rpl) },
	{ 0xda9e8977, __VMLINUX_SYMBOL_STR(cxgbi_sock_rcv_close_conn_rpl) },
	{ 0xb6544b8e, __VMLINUX_SYMBOL_STR(cxgbi_conn_pdu_ready) },
	{ 0xa5b2bbdc, __VMLINUX_SYMBOL_STR(___pskb_trim) },
	{ 0x418132dc, __VMLINUX_SYMBOL_STR(skb_copy_bits) },
	{ 0xa6dd1efc, __VMLINUX_SYMBOL_STR(cxgbi_sock_act_open_req_arp_failure) },
	{ 0x5fa90be3, __VMLINUX_SYMBOL_STR(cxgbi_sock_fail_act_open) },
	{ 0xc8fd727e, __VMLINUX_SYMBOL_STR(mod_timer) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x5b43fa2e, __VMLINUX_SYMBOL_STR(cxgb3_queue_tid_release) },
	{ 0xba63339c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_bh) },
	{ 0xa1b38c6a, __VMLINUX_SYMBOL_STR(cxgbi_conn_tx_open) },
	{ 0x9fd823ec, __VMLINUX_SYMBOL_STR(cxgbi_sock_established) },
	{ 0xcca27eeb, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0x1637ff0f, __VMLINUX_SYMBOL_STR(_raw_spin_lock_bh) },
	{ 0x3938cc69, __VMLINUX_SYMBOL_STR(cxgb3_free_atid) },
	{ 0xed1f022f, __VMLINUX_SYMBOL_STR(cxgb3_insert_tid) },
	{ 0xf570d9e5, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x60b3c597, __VMLINUX_SYMBOL_STR(t3_l2t_send_slow) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x35faeb3, __VMLINUX_SYMBOL_STR(cxgbi_ddp_ppod_set) },
	{ 0x5d2dc510, __VMLINUX_SYMBOL_STR(__alloc_skb) },
	{ 0x8682503a, __VMLINUX_SYMBOL_STR(cxgb3_ofld_send) },
	{ 0x28fe47c4, __VMLINUX_SYMBOL_STR(cxgbi_ddp_cleanup) },
	{ 0x53f51fd9, __VMLINUX_SYMBOL_STR(cxgbi_device_unregister) },
	{ 0x444b68a5, __VMLINUX_SYMBOL_STR(cxgbi_device_find_by_lldev) },
	{ 0x399b261a, __VMLINUX_SYMBOL_STR(__kfree_skb) },
	{ 0xc379fdf2, __VMLINUX_SYMBOL_STR(cxgbi_sock_rcv_wr_ack) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libiscsi,libcxgbi,libiscsi_tcp,cxgb3";


MODULE_INFO(srcversion, "F2642E5DBA53004A943CA27");
