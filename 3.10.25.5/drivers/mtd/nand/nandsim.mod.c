#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xe5d95985, __VMLINUX_SYMBOL_STR(param_ops_ulong) },
	{ 0xc1eb0c37, __VMLINUX_SYMBOL_STR(single_release) },
	{ 0x9c39470d, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0xd2bcdb9a, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0x4845c423, __VMLINUX_SYMBOL_STR(param_array_ops) },
	{ 0x35b6b772, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0x6d044c26, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x51bc3a46, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0x9e7d6bd0, __VMLINUX_SYMBOL_STR(__udelay) },
	{ 0x7a188791, __VMLINUX_SYMBOL_STR(prandom_bytes) },
	{ 0xa735db59, __VMLINUX_SYMBOL_STR(prandom_u32) },
	{ 0x161113ca, __VMLINUX_SYMBOL_STR(kernel_write) },
	{ 0x6ae4de83, __VMLINUX_SYMBOL_STR(kernel_read) },
	{ 0x4b1f5a52, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x67f43102, __VMLINUX_SYMBOL_STR(unlock_page) },
	{ 0x2e710893, __VMLINUX_SYMBOL_STR(put_page) },
	{ 0x1a8ea061, __VMLINUX_SYMBOL_STR(write_inode_now) },
	{ 0xb5c5b29d, __VMLINUX_SYMBOL_STR(find_or_create_page) },
	{ 0x52793124, __VMLINUX_SYMBOL_STR(find_get_page) },
	{ 0x1f7b8a42, __VMLINUX_SYMBOL_STR(debugfs_create_dir) },
	{ 0x1f3452f6, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x2dc0ab23, __VMLINUX_SYMBOL_STR(mtd_device_parse_register) },
	{ 0xd44d8538, __VMLINUX_SYMBOL_STR(mtd_block_markbad) },
	{ 0xb2b23a70, __VMLINUX_SYMBOL_STR(nand_default_bbt) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0xd6ee688f, __VMLINUX_SYMBOL_STR(vmalloc) },
	{ 0x40a9b349, __VMLINUX_SYMBOL_STR(vzalloc) },
	{ 0x7412f17b, __VMLINUX_SYMBOL_STR(filp_open) },
	{ 0x224c011d, __VMLINUX_SYMBOL_STR(nand_flash_ids) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xe9dd0854, __VMLINUX_SYMBOL_STR(nand_scan_tail) },
	{ 0xdefc1c6a, __VMLINUX_SYMBOL_STR(nand_scan_ident) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x20000329, __VMLINUX_SYMBOL_STR(simple_strtoul) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xc499ae1e, __VMLINUX_SYMBOL_STR(kstrdup) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x5f2ea640, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0xaa8b4be9, __VMLINUX_SYMBOL_STR(single_open) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xfb95090c, __VMLINUX_SYMBOL_STR(nand_release) },
	{ 0x1a908684, __VMLINUX_SYMBOL_STR(debugfs_remove_recursive) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0xc9409b91, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0x5fad3ab9, __VMLINUX_SYMBOL_STR(filp_close) },
	{ 0x999e8297, __VMLINUX_SYMBOL_STR(vfree) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mtd,nand,nand_ids";


MODULE_INFO(srcversion, "CF279F2FF1EEC38703CDFF9");
