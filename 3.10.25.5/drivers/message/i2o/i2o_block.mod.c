#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x348ba56c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x5a3ccad2, __VMLINUX_SYMBOL_STR(i2o_driver_register) },
	{ 0x71a50dbc, __VMLINUX_SYMBOL_STR(register_blkdev) },
	{ 0x3f71faa1, __VMLINUX_SYMBOL_STR(mempool_create) },
	{ 0x183fa88b, __VMLINUX_SYMBOL_STR(mempool_alloc_slab) },
	{ 0x8a99a016, __VMLINUX_SYMBOL_STR(mempool_free_slab) },
	{ 0x67cc86e8, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0xee558c53, __VMLINUX_SYMBOL_STR(dma_ops) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x3ddcdebf, __VMLINUX_SYMBOL_STR(blk_end_request_all) },
	{ 0xa15a9539, __VMLINUX_SYMBOL_STR(blk_end_request) },
	{ 0x7e743260, __VMLINUX_SYMBOL_STR(i2o_cntxt_list_get) },
	{ 0x8f64aa4, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0xbf899fdb, __VMLINUX_SYMBOL_STR(blk_start_queue) },
	{ 0x9327f5ce, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xfa801eb, __VMLINUX_SYMBOL_STR(blk_peek_request) },
	{ 0x92c962b, __VMLINUX_SYMBOL_STR(__blk_end_request_all) },
	{ 0xa2cd8a7a, __VMLINUX_SYMBOL_STR(blk_stop_queue) },
	{ 0x53f01b6b, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xfa2bcf10, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0xe94b430c, __VMLINUX_SYMBOL_STR(blk_start_request) },
	{ 0xc72b0927, __VMLINUX_SYMBOL_STR(i2o_cntxt_list_remove) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0x65eaba52, __VMLINUX_SYMBOL_STR(i2o_dma_map_sg) },
	{ 0x51e8a6e0, __VMLINUX_SYMBOL_STR(blk_rq_map_sg) },
	{ 0xe3deabb9, __VMLINUX_SYMBOL_STR(i2o_cntxt_list_add) },
	{ 0x4aad52d7, __VMLINUX_SYMBOL_STR(mempool_free) },
	{ 0xc897c382, __VMLINUX_SYMBOL_STR(sg_init_table) },
	{ 0xe9dff136, __VMLINUX_SYMBOL_STR(mempool_alloc) },
	{ 0xc813450, __VMLINUX_SYMBOL_STR(add_disk) },
	{ 0x5ce6d1ea, __VMLINUX_SYMBOL_STR(blk_queue_logical_block_size) },
	{ 0xab3c0014, __VMLINUX_SYMBOL_STR(i2o_parm_field_get) },
	{ 0x6a6feb36, __VMLINUX_SYMBOL_STR(blk_queue_max_segments) },
	{ 0x6f91d329, __VMLINUX_SYMBOL_STR(i2o_sg_tablesize) },
	{ 0xd8f9f68c, __VMLINUX_SYMBOL_STR(blk_queue_max_hw_sectors) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x92387997, __VMLINUX_SYMBOL_STR(blk_queue_prep_rq) },
	{ 0x62d06521, __VMLINUX_SYMBOL_STR(blk_init_queue) },
	{ 0xbf06b565, __VMLINUX_SYMBOL_STR(alloc_disk) },
	{ 0xcd758037, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x3f862391, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x8e8acc69, __VMLINUX_SYMBOL_STR(i2o_device_claim) },
	{ 0x56b98fee, __VMLINUX_SYMBOL_STR(i2o_msg_post_wait_mem) },
	{ 0x55dd99b5, __VMLINUX_SYMBOL_STR(i2o_msg_get_wait) },
	{ 0x61731c36, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xb2fd5ceb, __VMLINUX_SYMBOL_STR(__put_user_4) },
	{ 0xa1c76e0a, __VMLINUX_SYMBOL_STR(_cond_resched) },
	{ 0xcb0a263d, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x430a89d7, __VMLINUX_SYMBOL_STR(put_disk) },
	{ 0x2e133e3e, __VMLINUX_SYMBOL_STR(blk_cleanup_queue) },
	{ 0x4fe4c49e, __VMLINUX_SYMBOL_STR(i2o_device_claim_release) },
	{ 0x34cae5e5, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x777d1ee3, __VMLINUX_SYMBOL_STR(del_gendisk) },
	{ 0x4bc46530, __VMLINUX_SYMBOL_STR(i2o_event_register) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xb7131fa6, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xb294dd6, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0xefba93e1, __VMLINUX_SYMBOL_STR(mempool_destroy) },
	{ 0xb5a459dc, __VMLINUX_SYMBOL_STR(unregister_blkdev) },
	{ 0xae40d1cd, __VMLINUX_SYMBOL_STR(i2o_driver_unregister) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=i2o_core";


MODULE_INFO(srcversion, "9D1542D1807735EB3153008");
